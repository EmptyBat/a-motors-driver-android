package com.batman.almas.a_motors_driver;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.batman.almas.a_motors_driver.Adapter.OrdersAdapter;
import com.batman.almas.a_motors_driver.Model.OrdersModel;
import com.batman.almas.a_motors_driver.Vendor.BackgroundModeGPS;
import com.batman.almas.a_motors_driver.Vendor.Const;
import com.google.gson.JsonObject;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.PrivateChannel;
import com.pusher.client.channel.PrivateChannelEventListener;
import com.pusher.client.channel.SubscriptionEventListener;
import com.pusher.client.connection.ConnectionState;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import fr.quentinklein.slt.LocationTracker;
import fr.quentinklein.slt.TrackerSettings;

public class OrdersActivity extends AppCompatActivity implements  OrdersAdapter.PostsAdapterListener , NavigationView.OnNavigationItemSelectedListener  {
    public static final String TAG = OrdersActivity.class.getSimpleName();

    @BindView(R.id.order_switch)
    Switch order_switch;
    @BindView(R.id.order_driver_status_tv)
    TextView status_tv;
    @BindView(R.id.menu_btn_main)
    Button menu_btn;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.orders_recyclerView)
    RecyclerView recyclerView;
    List<OrdersModel> posts;
    OrdersAdapter adapter;
    SharedPreferences prefs;
    private boolean inBackground = true;
    LocationManager mLocationManager;
    Pusher pusher;
    PrivateChannel channel;
    PrivateChannel locationChannel;
    JSONObject jsonLocation;
    int status_id = 2;
    private Timer status_updater;
    Location driver_location;
    LocationTracker tracker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidNetworking.initialize(getApplicationContext());
        setContentView(R.layout.activity_drawer_order);
        ButterKnife.bind(this);
        prefs = getSharedPreferences(Preferences.shPref, 0);
        driver_location = getLocation();
        TrackerSettings settings =
                new TrackerSettings()
                        .setUseGPS(true)
                        .setUseNetwork(true)
                        .setUsePassive(true)
                        .setTimeBetweenUpdates(Integer.valueOf(prefs.getString(Preferences.frequency_without_order, null)))
                        .setMetersBetweenUpdates(Integer.valueOf(prefs.getString(Preferences.frequency_by_distance, null)));
        tracker = new LocationTracker(this, settings) {
            @Override
            public void onTimeout() {
                Log.d("LocationTracker", "onTimeout");
            }
            @Override
            public void onLocationFound(Location location) {
                driver_location = location;
                sendLocationToPusher();
                Log.d("LocationTracker", "onLocationFound");
            }
        };
        stopService(new Intent(this,BackgroundModeGPS.class));

        tracker.startListening();
        posts = new ArrayList<>();
        setNavigationDrawer();
        prepareUI();
        handleData();
        createPusher();
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("driver_status",status_id);
        editor.commit();
        SharedPreferences pref_default = PreferenceManager.getDefaultSharedPreferences(this);
            boolean driveIsBusy = pref_default.getBoolean("driveIsBusy", false);
            if (driveIsBusy == true) {
            status_id = 4;
        }
        else {
            if (prefs.getInt(Preferences.driverStatus, 2) == 2) {
                status_tv.setText("Готов");
                status_id = 2;
                order_switch.setChecked(true);
            }
            else {
                status_tv.setText("Недоступен");
                status_id = 3;
                order_switch.setChecked(false);
            }
        }
        status_updater = new Timer();
        status_updater.schedule(new TimerTask() {
            @Override
            public void run() {
                DriverStatusChanged(String.valueOf(status_id));
            }
        }, 0, 60000);
        order_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                      status_tv.setText("Готов");
                    status_id = 2;
                    DriverStatusChanged("2");
                    SharedPreferences shared = getSharedPreferences(Preferences.shPref, 0);
                    SharedPreferences.Editor edit = shared.edit();
                    edit.putInt(Preferences.driverStatus,2);
                    edit.commit();
                    sendLocationToPusher();
                } else {
                    status_id = 3;
                    status_tv.setText("Недоступен");
                    DriverStatusChanged("3");
                    SharedPreferences shared = getSharedPreferences(Preferences.shPref, 0);
                    SharedPreferences.Editor edit = shared.edit();
                    edit.putInt(Preferences.driverStatus,3);
                    edit.commit();
                    sendLocationToPusher();
                }
            }
        });
    }
    private Location getLocation(){
        mLocationManager = (LocationManager)getApplicationContext().getSystemService(LOCATION_SERVICE);
        List<String> providers = mLocationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            Location l = mLocationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }
        return bestLocation;
    }
    private void prepareUI() {
        if (ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 200);
                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
        if (checkLocationExternalPermission()) {
            Log.d("checkLocationExternalPermission","checkLocationExternalPermission");
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

        }
        else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }
    }
    private boolean checkLocationExternalPermission()
    {
        String permission = android.Manifest.permission.ACCESS_FINE_LOCATION;
        int res = getApplicationContext().checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 200 : {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    Toast.makeText(getApplicationContext(), "Permission granted", Toast.LENGTH_SHORT).show();
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(getApplicationContext(), "Permission denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
    private void sendLocationToPusher(){
        if (pusher.getConnection().getState() == ConnectionState.CONNECTED) {
            if (locationChannel.isSubscribed()) {
                    jsonLocation = new JSONObject();
                    JSONObject location = new JSONObject();
                    try {
                        if (driver_location.getLongitude() != 0) {
                            location.put("longitude", driver_location.getLongitude());
                            location.put("latitude", driver_location.getLatitude());
                            jsonLocation.put("location", location);
                            jsonLocation.put("driver_id", prefs.getString(Preferences.user_id, null));
                            jsonLocation.put("status_id", status_id);
                            jsonLocation.put("car_number", prefs.getString(Preferences.car_id, null));
                            locationChannel.trigger("client-location-updated", jsonLocation.toString());
                        }
                    } catch (JSONException e) {
                    }

            }
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
       if (inBackground == true) {
        getOrders();
        inBackground = false;
            }
        stopService(new Intent(this,BackgroundModeGPS.class));

    }
    @Override
    public void onPause()
    {
        super.onPause();
        inBackground = true;
        startService(new Intent(this,BackgroundModeGPS .class));
    }

    private void getPermessions(){
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
    }
    private void handleData(){
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String driver_id = bundle.getString("driver_id");
            String status_id = bundle.getString("status_id");
            if (status_id.equals("4")||status_id.equals("5")) {
                Intent intent = new Intent(getBaseContext(), OrdersActivity.class);
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean("driveIsBusy",false);
                SharedPreferences shared = getSharedPreferences(Preferences.shPref, 0);
                SharedPreferences.Editor edit = shared.edit();
                edit.putBoolean(Preferences.arrivedTo,false);
                editor.commit();
                edit.commit();
                startActivity(intent);
            }
            else {
                if (prefs.getString(Preferences.user_id, null).equals(driver_id)) {
                    if (status_updater != null) {
                        status_updater.cancel();
                        status_updater = null;
                    }
                    tracker.stopListening();
                    if (pusher !=null) {
                        pusher.disconnect();
                    }
                    Intent intent = new Intent(getBaseContext(), MainActivity.class);
                    intent.putExtra("ORDER_ID", bundle.getString("order_id"));
                    intent.putExtra("PASSANGER_NAME",bundle.getString("passanger_name"));
                    intent.putExtra("DRIVER_ID",bundle.getString("driver_id"));
                    startActivity(intent);

                }
            }
        }
    }
    private void createPusher() {
        HashMap<String, String> authHeader = new HashMap<>();
        authHeader.put("Authorization", "Basic ZGV2QGFtb3RvcnMua3o6MTIzMTIz");
        final com.batman.almas.a_motors_driver.HttpAuthorizer authorizer = new  com.batman.almas.a_motors_driver.HttpAuthorizer(
                Const.PUSHER_AUTH_URL );
        authorizer.setHeaders(authHeader);
        PusherOptions options = new PusherOptions().setCluster("ap2").setAuthorizer(authorizer).setEncrypted(true);
        pusher = new Pusher("7ea5886145e8af112d5f", options);
        Channel channel = pusher.subscribe("api-orders-pull");
        channel.bind("api-orders-pull.created", new SubscriptionEventListener() {
            @Override
            public void onEvent(String channelName, String eventName, final String data) {
                System.out.println(data);
                try {
                    JSONObject obj = new JSONObject(data);
                    obj = obj.getJSONObject("order");
                    String order_id = obj.getString("id");
                    newOrder(obj,order_id);
                    Log.d("My App", obj.toString());

                } catch (Throwable t) {
                }
            }
        });
        channel.bind("api-orders-pull.updated", new SubscriptionEventListener() {
            @Override
            public void onEvent(String channelName, String eventName, final String data) {
                System.out.println(data);
                try {

                    JSONObject obj = new JSONObject(data);
                    obj = obj.getJSONObject("order");
                    String order_id = obj.getString("id");
                    updatedOrder(obj,order_id);
                    Log.d("My App", obj.toString());

                } catch (Throwable t) {
                }
            }
        });
        channel.bind("api-orders-pull.accepted", new SubscriptionEventListener() {
            @Override
            public void onEvent(String channelName, String eventName, final String data) {
                System.out.println(data);
                try {

                    JSONObject obj = new JSONObject(data);
                    obj = obj.getJSONObject("order");
                    String order_id = obj.getString("id");
                    removeOrder(obj,order_id);
                    Log.d("My accepted", obj.toString());

                } catch (Throwable t) {
                }
            }
        });
        channel.bind("api-orders-pull.declied", new SubscriptionEventListener() {
            @Override
            public void onEvent(String channelName, String eventName, final String data) {
                System.out.println(data);
                try {

                    JSONObject obj = new JSONObject(data);
                    obj = obj.getJSONObject("order");
                    String order_id = obj.getString("id");
                    removeOrder(obj,order_id);
                    Log.d("My declied", obj.toString());

                } catch (Throwable t) {
                }
            }
        });
        channel.bind("api-orders-pull.canceled", new SubscriptionEventListener() {
            @Override
            public void onEvent(String channelName, String eventName, final String data) {
                System.out.println(data);
                try {
                    JSONObject obj = new JSONObject(data);
                    obj = obj.getJSONObject("order");
                    String order_id = obj.getString("id");
                    removeOrder(obj,order_id);
                    Log.d("My canceled", obj.toString());

                } catch (Throwable t) {
                }
            }
        });
        pusher.connect();
        locationChannel = pusher.subscribePrivate("private-driver-location", new PrivateChannelEventListener() {
            @Override
            public void onSubscriptionSucceeded(String s) {
                Log.d(TAG, "sss");
                jsonLocation = new JSONObject();
                JSONObject location = new JSONObject();
                try {
                    if (driver_location.getLongitude() != 0) {
                        location.put("longitude", driver_location.getLongitude());
                        location.put("latitude", driver_location.getLatitude());
                        jsonLocation.put("location", location);
                        jsonLocation.put("driver_id", prefs.getString(Preferences.user_id, null));
                        jsonLocation.put("status_id", status_id);
                        jsonLocation.put("car_number", prefs.getString(Preferences.car_id, null));
                        locationChannel.trigger("client-location-updated", jsonLocation.toString());
                    }

                } catch (JSONException e) {

                }
            }
            @Override
            public void onAuthenticationFailure(String s, Exception e) {
                Log.d(TAG, "Failure "+ s + e.getLocalizedMessage());
            }
            @Override
            public void onEvent(String s, String s1, String s2) {
                Log.d(TAG, "fall ");
            }
        });
        Log.d(TAG, "createPusher: " + locationChannel);
    }
    private void updatedOrder(JSONObject order,String order_id) {
        for (int a = 0; a < posts.size(); a++) {
            if (posts.get(a).getOrder_id() == order_id) {
                try {
                    OrdersModel categoryItemModel = new OrdersModel();
                    categoryItemModel.setOrder_id(order.getString("id"));
                    categoryItemModel.setComment(order.getString("comment"));
                    categoryItemModel.setPassanger_name(order.getString("passanger_name"));
                    categoryItemModel.setData_from_name(order.getJSONObject("data_from").getString("name"));
                    categoryItemModel.setData_to_name(order.getJSONObject("data_to").getString("name"));
                    categoryItemModel.setOrder_ate(order.getString("order_at"));
                    posts.add(a, categoryItemModel);

                } catch (Throwable t) {
                }
            }
        }
        runOnUiThread(new Runnable() {
            public void run() {
                if (recyclerView.getAdapter() != null) {
                    recyclerView.getAdapter().notifyDataSetChanged();
                }
                else {
                    adapter = new OrdersAdapter(posts,OrdersActivity.this);
                    LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                    recyclerView.setLayoutManager(layoutManager);
                    recyclerView.setAdapter(adapter);
                }
            }
        });
    }
    private void newOrder(JSONObject order,String order_id){
                try {
                    OrdersModel categoryItemModel = new OrdersModel();
                    categoryItemModel.setOrder_id(order.getString("id"));
                    categoryItemModel.setComment(order.getString("comment"));
                    categoryItemModel.setPassanger_name(order.getString("passanger_name"));
                    categoryItemModel.setData_from_name(order.getJSONObject("data_from").getString("name"));
                    categoryItemModel.setData_to_name(order.getJSONObject("data_to").getString("name"));
                    categoryItemModel.setOrder_ate(order.getString("order_at"));
                    posts.add(0,categoryItemModel);

                } catch (Throwable t) {
                }
        runOnUiThread(new Runnable() {
            public void run() {
               // recyclerView.getAdapter().notifyDataSetChanged();
            }
        });
        getOrders();
    }
    private void removeOrder(JSONObject order,String order_id){
        for (int a = 0;a<posts.size();a++) {
            if (posts.get(a).getOrder_id() == order_id){
                try {
                    posts.remove(a);
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                        }
                    }, 1500);

                } catch (Throwable t) {
                }
            }
        }
        runOnUiThread(new Runnable() {
            public void run() {
              //  recyclerView.getAdapter().notifyDataSetChanged();
            }
        });
        getOrders();
    }
    private void setNavigationDrawer() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, null, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_force_remove).setVisible(false);
        View headerView = navigationView.getHeaderView(0);
        TextView navUsername = (TextView) headerView.findViewById(R.id.name_tv);
        TextView navPhone = (TextView) headerView.findViewById(R.id.phone_tv);
        CircleImageView navImage = (CircleImageView) headerView.findViewById(R.id.profile_image);
        navUsername.setText(prefs.getString(Preferences.userName, null));
        navPhone.setText(prefs.getString(Preferences.userPhone, null));
        Picasso.get()
                .load(Const.DOMAIN_URL  +prefs.getString(Preferences.image_url, null) )
                .into(navImage);
        drawer.closeDrawer(Gravity.LEFT);
    }
    private void getOrders() {
        String url = String.format(Const.ORDERS_URL,"1",prefs.getString(Preferences.user_id, null));
        AndroidNetworking.get(url)
                .addHeaders("Authorization","Basic ZGV2QGFtb3RvcnMua3o6MTIzMTIz")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            posts.clear();
                            JSONArray data = response.getJSONArray("data");
                            Log.e(TAG,"index "+ data);
                            for(int i = 0; i < data.length(); i++){
                                JSONObject OrderItem = data.getJSONObject(i);
                                OrdersModel categoryItemModel = new OrdersModel();
                                categoryItemModel.setOrder_id(OrderItem.getString("id"));
                                categoryItemModel.setComment(OrderItem.getString("comment"));
                                categoryItemModel.setPassanger_name(OrderItem.getString("passanger_name"));
                                categoryItemModel.setData_from_name(OrderItem.getJSONObject("data_from").getString("name"));
                                categoryItemModel.setData_to_name(OrderItem.getJSONObject("data_to").getString("name"));
                                categoryItemModel.setCompany_name(OrderItem.getJSONObject("company").getString("title"));
                                categoryItemModel.setOrder_ate(OrderItem.getString("order_at"));
                                categoryItemModel.setPreliminary(OrderItem.getBoolean("preliminary"));
                                if (OrderItem.getBoolean("preliminary") == true) {
                                    if (i != 0) {
                                        posts.add(posts.get(0));
                                        posts.set(0,categoryItemModel);
                                    }
                                    else {
                                        posts.add(categoryItemModel);
                                    }
                                }
                                else {
                                    posts.add(categoryItemModel);
                                }
                            }
                            adapter = new OrdersAdapter(posts,OrdersActivity.this);
                            LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                            recyclerView.setLayoutManager(layoutManager);
                            recyclerView.setAdapter(adapter);
                            recyclerView.getAdapter().notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        Log.i(TAG, anError.getErrorBody().toString());
                        Log.i(TAG, anError.getErrorDetail().toString());
                        Log.i(TAG, String.valueOf(anError.getErrorCode()));
//                        Log.i(TAG, anError.getResponse().toString());
                        Toast.makeText(OrdersActivity.this, getString(R.string.bad_internet_connection), Toast.LENGTH_SHORT).show();
                        Log.i(TAG, "HERE2");
                    }
                });
    }
    public void onModelDataselected(OrdersModel post) {
        if (post.getPreliminary() == false) {
            status_updater.cancel();
            tracker.stopListening();
            pusher.disconnect();
            Intent intent = new Intent(getBaseContext(), MainActivity.class);
            intent.putExtra("ORDER_ID", post.getOrder_id());
            intent.putExtra("PASSANGER_NAME", post.getPassanger_name());
            startActivity(intent);
        }
        else {
            Toast.makeText(OrdersActivity.this, getString(R.string.preliminary_order), Toast.LENGTH_SHORT).show();
        }
    }
    public void DriverStatusChanged(String status_id) {
        SharedPreferences pref_default = PreferenceManager.getDefaultSharedPreferences(this);
        boolean driveIsBusy = pref_default.getBoolean("driveIsBusy", false);
        if (driveIsBusy == false) {
            Map<String, String> params = new HashMap<>();
            params.put("status_id", status_id);
            params.put("latitude", String.valueOf(driver_location.getLatitude()));
            params.put("longitude", String.valueOf(driver_location.getLongitude()));
            AndroidNetworking.post(Const.BASE_URL + "drivers/" + prefs.getString(Preferences.user_id, null) + "/change_status")
                    .addHeaders("Authorization", "Basic ZGV2QGFtb3RvcnMua3o6MTIzMTIz")
                    .addBodyParameter(params)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                        }
                        @Override
                        public void onError(ANError anError) {
                            Log.i(TAG, anError.getErrorBody().toString());
                            Log.i(TAG, anError.getErrorDetail().toString());
                            Log.i(TAG, String.valueOf(anError.getErrorCode()));
                            Log.i(TAG, anError.getResponse().toString());
                            Log.i(TAG, "HERE2");
                        }
                    });
        }}

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_history) {
            Intent intent = new Intent(getBaseContext(), HistoryActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_dispatcher) {

        } else if (id == R.id.nav_exit) {
            SharedPreferences sharedPreferences = getSharedPreferences(Preferences.shPref, 0);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(Preferences.user_id, null);
            editor.commit();
            Intent newAct = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(newAct);
            finish();
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    @OnClick(R.id.menu_btn_main)
    public void toogle_btn(){
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.openDrawer(GravityCompat.START);
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


}
