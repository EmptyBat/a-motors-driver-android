package com.batman.almas.a_motors_driver.Model;

import android.graphics.Color;

import java.io.Serializable;
import java.util.List;

public class DetailTripModel implements Serializable
{
    public static final int COMPANY_NAME_TITLE=0;
    public static final int DISTRICT_FROM_TYPE=1;
    public static final int ADDITIONA_DISTRICT_TYPE=2;
    public static final int DISTRICT_TO_TYPE=3;
    public static final int WAITING_TITLE_TYPE=4;
    public static final int WAITING_TYPE=5;
    public static final int ABOUT_TAXI=6;

    private String order_id;
    private String is_drive_by_fact;
    private String comment;
    private String passanger_name;
    private String data_from_name;
    private String data_to_name;
    private String company_name;
    private String order_at;
    private String finished_at;
    private String started_at;
    private String time_total;
    private String time_drive;
    private String time_wait;
    private String distance;
    private Integer district_type;
    private List<String> districts;

    public DetailTripModel(){}

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getIs_drive_by_fact() {
        return is_drive_by_fact;
    }

    public void setIs_drive_by_fact(String is_drive_by_fact) {
        this.is_drive_by_fact = is_drive_by_fact;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getPassanger_name() {
        return passanger_name;
    }

    public void setPassanger_name(String passanger_name) {
        this.passanger_name = passanger_name;
    }

    public String getData_from_name() {
        return data_from_name;
    }

    public void setData_from_name(String data_from_name) {
        this.data_from_name = data_from_name;
    }

    public String getData_to_name() {
        return data_to_name;
    }

    public void setData_to_name(String data_to_name) {
        this.data_to_name = data_to_name;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getOrder_at() {
        return order_at;
    }

    public void setOrder_ate(String order_at) {
        this.order_at = order_at;
    }

    public Integer getDistrict_type() {
        return district_type;
    }

    public void setDistrict_type(Integer district_type) {
        this.district_type = district_type;
    }

    public List<String> getDistricts() {
        return districts;
    }

    public void setDistricts(List<String>  districts) {
        this.districts = districts;
    }

    public String getFinished_at() {
        return finished_at;
    }

    public void setFinished_at(String finished_at) {
        this.finished_at = finished_at;
    }

    public String getStarted_at() {
        return started_at;
    }

    public void setStarted_at(String started_at) {
        this.started_at = started_at;
    }
    public String getTime_total() {
        return time_total;
    }

    public void setTime_total(String time_total) {
        this.time_total = time_total;
    }
    public String getTime_drive() {
        return time_drive;
    }

    public void setTime_drive(String time_drive) {
        this.time_drive = time_drive;
    }

    public String getTime_wait() {
        return time_wait;
    }

    public void setTime_wait(String time_wait) {
        this.time_wait = time_wait;
    }
    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }



    public DetailTripModel(int type)
    {
        this.district_type=type;


    }


}

