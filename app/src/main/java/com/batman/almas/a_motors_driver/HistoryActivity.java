package com.batman.almas.a_motors_driver;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.batman.almas.a_motors_driver.Adapter.SectionPageAdapter;
import com.batman.almas.a_motors_driver.Fragment.HistoryFragment;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class HistoryActivity extends AppCompatActivity {
    private static final String TAG = "HistoryActivity";

    private SectionPageAdapter mSectionsPageAdapter;

    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        ButterKnife.bind(this);
        Log.d(TAG, "onCreate: Starting.");

        mSectionsPageAdapter = new SectionPageAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        setupViewPager(mViewPager);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        tabLayout.setTabTextColors(ContextCompat.getColor(getApplicationContext(),R.color.whiteColor),ContextCompat.getColor(getApplicationContext(),R.color.LightGrayColor));
    }

    private void setupViewPager(ViewPager viewPager) {
        SectionPageAdapter adapter = new SectionPageAdapter(getSupportFragmentManager());
        adapter.addFragment(new HistoryFragment(), "Сегодня");
        adapter.addFragment(new HistoryFragment(), "За 3 дня");
        adapter.addFragment(new HistoryFragment(), "За неделю");
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(1);
    }
    @OnClick(R.id.cancel_btn)
    public void cancel_btnClicked(){
        Log.d("work","");
        finish();
    }

}
