package com.batman.almas.a_motors_driver.Vendor;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.batman.almas.a_motors_driver.MainActivity;
import com.batman.almas.a_motors_driver.Preferences;
import com.batman.almas.a_motors_driver.R;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.PrivateChannel;
import com.pusher.client.channel.PrivateChannelEventListener;
import com.pusher.client.connection.ConnectionState;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.quentinklein.slt.LocationTracker;
import fr.quentinklein.slt.TrackerSettings;

public class BackgroundModeGPS extends Service
    {
        Pusher pusher;
        PrivateChannel channel;
        JSONObject jsonLocation;
        private static final String TAG = "BOOMBOOMTESTGPS";
        private LocationManager mLocationManager = null;
        Location mLastLocation;
        Location driver_location;
        LocationTracker tracker;
        public static final int NOTIFICATION_ID = 200;
        private class LocationListener implements android.location.LocationListener{
            public LocationListener(String provider)
            {
                Log.e(TAG, "LocationListener " + provider);
                mLastLocation = new Location(provider);
            }
            @Override
            public void onLocationChanged(Location location)
            {
              /*  SharedPreferences driver_pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                SharedPreferences prefs = getSharedPreferences(Preferences.shPref, 0);
                Log.e(TAG, "onLocationChanged: " + location);
                mLastLocation.set(location);
                if (pusher.getConnection().getState() == ConnectionState.CONNECTED) {
                    if (channel.isSubscribed()) {
                        jsonLocation = new JSONObject();
                        JSONObject locations = new JSONObject();
                        try {
                            locations.put("longitude", location.getLongitude());
                            locations.put("latitude", location.getLatitude());
                            jsonLocation.put("location", location);
                            jsonLocation.put("driver_id", prefs.getString(Preferences.user_id, null));
                            jsonLocation.put("status_id", driver_pref.getInt("driver_status",2));
                            jsonLocation.put("car_number", prefs.getString(Preferences.car_id, null));
                            channel.trigger("client-location-updated", jsonLocation.toString());

                        } catch (JSONException e) {
                        }
                    }

                }*/
            }
            @Override
            public void onProviderDisabled(String provider)
            {
                Log.e(TAG, "onProviderDisabled: " + provider);
            }
            @Override
            public void onProviderEnabled(String provider)
            {
                Log.e(TAG, "onProviderEnabled: " + provider);
            }
            @Override
            public void onStatusChanged(String provider, int status, Bundle extras)
            {
                Log.e(TAG, "onStatusChanged: " + provider);
            }
        }
        LocationListener[] mLocationListeners = new LocationListener[] {
                new LocationListener(LocationManager.GPS_PROVIDER),
                new LocationListener(LocationManager.NETWORK_PROVIDER)
        };
        @Override
        public IBinder onBind(Intent arg0)
        {
            return null;
        }
        @Override
        public int onStartCommand(Intent intent, int flags, int startId)
        {
            Log.e(TAG, "onStartCommand");
            super.onStartCommand(intent, flags, startId);
            return START_STICKY;
        }
        @Override
        public void onCreate()
        {    sendNotification(this, false);
            Log.e(TAG, "onCreate");
         //   initializeLocationManager();
            SharedPreferences prefs = getSharedPreferences(Preferences.shPref, 0);
            driver_location = getLocation();
            TrackerSettings settings =
                    new TrackerSettings()
                            .setUseGPS(true)
                            .setUseNetwork(true)
                            .setUsePassive(true)
                            .setTimeBetweenUpdates(Integer.valueOf(prefs.getString(Preferences.frequency_without_order, null)));
                           // .setMetersBetweenUpdates(Integer.valueOf(prefs.getString(Preferences.frequency_by_distance, null)));
            tracker = new LocationTracker(this, settings) {

                @Override
                public void onTimeout() {
                    Log.d("LocationTracker", "onTimeout");
                }
                @Override
                public void onLocationFound(Location location) {
                    driver_location = location;
                    sendLocationToPusher();
                    Log.d("LocationTracker", "onLocationFound");
                }
            };
            tracker.startListening();
            createPusher();
        }
        private void sendLocationToPusher(){
            SharedPreferences driver_pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences prefs = getSharedPreferences(Preferences.shPref, 0);
            Log.e(TAG, "onLocationChanged: " + driver_location);
            mLastLocation.set(driver_location);
            if (pusher.getConnection().getState() == ConnectionState.CONNECTED) {
                if (channel.isSubscribed()) {
                    jsonLocation = new JSONObject();
                    JSONObject locations = new JSONObject();
                    try {
                        if (driver_location.getLongitude() != 0) {
                            locations.put("longitude", driver_location.getLongitude());
                            locations.put("latitude", driver_location.getLatitude());
                            jsonLocation.put("location", locations);
                            jsonLocation.put("driver_id", prefs.getString(Preferences.user_id, null));
                            jsonLocation.put("status_id", driver_pref.getInt("driver_status", 2));
                            jsonLocation.put("car_number", prefs.getString(Preferences.car_id, null));
                            channel.trigger("client-location-updated", jsonLocation.toString());
                        }

                    } catch (JSONException e) {
                    }
                }
            }
        }
        @Override
        public void onDestroy()
        {
            Log.e(TAG, "onDestroy");
            super.onDestroy();
            if (mLocationManager != null) {
                for (int i = 0; i < mLocationListeners.length; i++) {
                    try {
                        mLocationManager.removeUpdates(mLocationListeners[i]);
                    } catch (Exception ex) {
                        Log.i(TAG, "fail to remove location listners, ignore", ex);
                    }
                }
            }
        }
        private void initializeLocationManager() {
        Log.e(TAG, "initializeLocationManager");
        if (mLocationManager == null) {
            mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }

    }
        private Location getLocation(){
            mLocationManager = (LocationManager)getApplicationContext().getSystemService(LOCATION_SERVICE);
            List<String> providers = mLocationManager.getProviders(true);
            Location bestLocation = null;
            for (String provider : providers) {
                Location l = mLocationManager.getLastKnownLocation(provider);
                if (l == null) {
                    continue;
                }
                if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                    // Found best last known location: %s", l);
                    bestLocation = l;
                }
            }
            return bestLocation;
        }
        public static void sendNotification(Service service, boolean isUpdate) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                Intent intent = new Intent(service, MainActivity.class);
                PendingIntent pendingIntent = PendingIntent.getActivity(service, 0, intent, PendingIntent.FLAG_NO_CREATE);
                NotificationCompat.Builder mNotifyBuilder = new NotificationCompat.Builder(service)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("INFO_NOTIFICATION_TITLE")
                        .setOngoing(true)
                        .setAutoCancel(false)
                        .setContentText("INFO_NOTIFICATION_MESSAGE")
                        .setContentIntent(pendingIntent);
                Notification notification = mNotifyBuilder.build();

                if (isUpdate) {
                    NotificationManager notificationManager = (NotificationManager) service.getSystemService(NOTIFICATION_SERVICE);
                    if (notificationManager != null) {
                        notificationManager.notify(NOTIFICATION_ID, notification);
                    }
                } else {
                    service.startForeground(NOTIFICATION_ID, notification);
                }
            }
        }
        private void createPusher() {
            HashMap<String, String> authHeader = new HashMap<>();
            authHeader.put("Authorization", "Basic ZGV2QGFtb3RvcnMua3o6MTIzMTIz");
            final com.batman.almas.a_motors_driver.HttpAuthorizer authorizer = new  com.batman.almas.a_motors_driver.HttpAuthorizer(
                    Const.PUSHER_AUTH_URL );
            authorizer.setHeaders(authHeader);
            PusherOptions options = new PusherOptions().setCluster("ap2").setAuthorizer(authorizer).setEncrypted(true);
            pusher = new Pusher("1f517ad8f9e6adf92d0a", options);
            pusher.connect();
            channel = pusher.subscribePrivate("private-driver-location", new PrivateChannelEventListener() {
                @Override
                public void onSubscriptionSucceeded(String s) {
                    Log.d(TAG, "sss");
                    jsonLocation = new JSONObject();
                    JSONObject location = new JSONObject();
                    SharedPreferences driver_pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    try {
                        if (mLastLocation.getLongitude() != 0) {
                            SharedPreferences prefs = getSharedPreferences(Preferences.shPref, 0);
                            location.put("longitude", mLastLocation.getLongitude());
                            location.put("latitude", mLastLocation.getLatitude());
                            jsonLocation.put("location", location);
                            jsonLocation.put("driver_id", prefs.getString(Preferences.user_id, null));
                            jsonLocation.put("status_id", driver_pref.getInt("driver_status", 2));
                            jsonLocation.put("car_number", prefs.getString(Preferences.car_id, null));
                            channel.trigger("client-location-updated", jsonLocation.toString());
                        }

                    } catch (JSONException e) {

                    }
                }
                @Override
                public void onAuthenticationFailure(String s, Exception e) {
                    Log.d(TAG, "Failure "+ s + e.getLocalizedMessage());
                }
                @Override
                public void onEvent(String s, String s1, String s2) {
                    Log.d(TAG, "fall ");
                }
            });
            Log.d(TAG, "createPusher: " + channel);
        }
    }