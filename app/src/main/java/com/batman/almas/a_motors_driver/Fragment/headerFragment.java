package com.batman.almas.a_motors_driver.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.androidnetworking.AndroidNetworking;
import com.batman.almas.a_motors_driver.MainActivity;
import com.batman.almas.a_motors_driver.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class headerFragment extends Fragment {
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        AndroidNetworking.initialize(getActivity());
        View view = inflater.inflate(R.layout.standart_header, container, false);
        ButterKnife.bind(this,view);
        return view;
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

}
