package com.batman.almas.a_motors_driver.Fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.batman.almas.a_motors_driver.Adapter.DetailTripAdapter;
import com.batman.almas.a_motors_driver.Adapter.HistoryAdapter;
import com.batman.almas.a_motors_driver.HistoryActivity;
import com.batman.almas.a_motors_driver.LoginActivity;
import com.batman.almas.a_motors_driver.Model.AboutTaxiModel;
import com.batman.almas.a_motors_driver.Model.DataCasualModel;
import com.batman.almas.a_motors_driver.Model.DetailTripModel;
import com.batman.almas.a_motors_driver.Model.DistrictList;
import com.batman.almas.a_motors_driver.Model.StatsModel;
import com.batman.almas.a_motors_driver.Preferences;
import com.batman.almas.a_motors_driver.R;
import com.batman.almas.a_motors_driver.TripDetailActivity;
import com.batman.almas.a_motors_driver.Vendor.Const;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HistoryFragment extends Fragment {
    @BindView(R.id.history_rv)
    RecyclerView recyclerView;
    private String position_name = "today";
    List<DetailTripModel> itemType =  new ArrayList<>();
    List<DistrictList> districtsList = new ArrayList<>();
    List<DetailTripModel> itemList =  new ArrayList<>();
    List<Integer> itemController =  new ArrayList<>();
    HistoryAdapter adapter;
    public static final String TAG = LoginActivity.class.getSimpleName();

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        AndroidNetworking.initialize(getActivity());
        View view = inflater.inflate(R.layout.fragment_history, container, false);
        ButterKnife.bind(this,view);
        adapter = new HistoryAdapter(itemList,itemType,districtsList,getActivity(),itemController);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        getHistoryinfo();
        return view;
    }
    public static Fragment getInstance(int position) {
        HistoryFragment f = new HistoryFragment();
        Bundle args = new Bundle();
        args.putInt("position", position);
        f.setArguments(args);
        return f;
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
    private String getDay(){
        int day = getArguments().getInt("position");
        String returned_value = "";
    switch (day) {
        case 0:
            returned_value = "today";
            break;
        case 1:
            returned_value = "3days";
            break;
        case 2:
            returned_value = "week";
        default:
    }
    return returned_value;
    }
    private void getHistoryinfo() {
        SharedPreferences prefs = getActivity().getSharedPreferences(Preferences.shPref, 0);
        itemType.clear();
        districtsList.clear();
        itemList.clear();
        position_name = getDay();
        String formatString = Const.ORDER_HISTORY_URL;
        String url = String.format(formatString,prefs.getString(Preferences.user_id, null),position_name);
        AndroidNetworking.get(url)
                .addHeaders("Authorization","Basic ZGV2QGFtb3RvcnMua3o6MTIzMTIz")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray json_data = response.getJSONArray("data");
                            for (int i = 0; i < json_data.length(); i++) {
                                itemController.add(i);
                                itemController.add(i+1);
                                JSONObject data = json_data.getJSONObject(i);
                                DetailTripModel DistrictItemModel = new DetailTripModel();
                                DistrictList districtList = new DistrictList();
                                Date currentTime = Calendar.getInstance().getTime();
                                DistrictItemModel.setCompany_name(data.getJSONObject("company").getString("title"));
                                DistrictItemModel.setPassanger_name(data.getString("passanger_name"));
                                DistrictItemModel.setStarted_at(data.getString("started_at"));
                                DistrictItemModel.setFinished_at(currentTime.toString());
                                if (data.get("information") instanceof JSONObject) {
                                    JSONObject information = data.getJSONObject("information").getJSONObject("exists");
                                    if (information.has("data_to")) {
                                        JSONObject stats_value = information.getJSONObject("data_to");
                                        if (stats_value.has("time")) {
                                            JSONObject stats = stats_value.getJSONObject("time");
                                            DistrictItemModel.setStarted_at(stats.getString("started"));
                                            DistrictItemModel.setFinished_at(stats.getString("finished"));
                                        } else {
                                            DistrictItemModel.setStarted_at(data.getString("started_at"));
                                            DistrictItemModel.setFinished_at(currentTime.toString());
                                        }
                                    }
                                }
                                itemList.add(DistrictItemModel);
                                itemType.add(new DetailTripModel(DetailTripModel.COMPANY_NAME_TITLE));
                                itemType.add(new DetailTripModel(DetailTripModel.DISTRICT_FROM_TYPE));
                                districtList.setLatitude(data.getJSONObject("data_from").getString("latitude"));
                                districtList.setLongitude(data.getJSONObject("data_from").getString("longitude"));
                                districtList.setName(data.getJSONObject("data_from").getString("name"));
                                districtList.setDistance("0");
                                districtList.setTime("0");
                                districtList.setOrder_id(data.getString("id"));
                                districtsList.add(districtList);

                                if (data.get("data_additional") instanceof JSONArray) {
                                    JSONArray data_additional = data.optJSONArray("data_additional");
                                    for (int j = 0; j < data_additional.length(); j++) {
                                        itemController.add(i + 1);
                                        districtList = new DistrictList();
                                        JSONObject jsonObject = data_additional.getJSONObject(j);
                                        districtList.setName(jsonObject.getString("name"));
                                        districtList.setLongitude(jsonObject.getString("longitude"));
                                        districtList.setLatitude(jsonObject.getString("latitude"));
                                        districtList.setOrder_id(data.getString("id"));
                                        itemType.add(new DetailTripModel(DetailTripModel.ADDITIONA_DISTRICT_TYPE));
                                        if (data.get("information") instanceof JSONObject) {
                                            JSONObject information = data.getJSONObject("information").getJSONObject("exists");
                                            if (information.has("data_additional_"+(j+1))) {
                                                JSONObject stats_value = information.getJSONObject("data_additional_"+(j+1));
                                                if (stats_value.has("stats")) {
                                                    JSONObject stats = stats_value.getJSONObject("stats");
                                                    districtList.setDistance(stats.getString("distance"));
                                                    districtList.setTime(stats.getString("time"));
                                                }
                                                else {
                                                    districtList.setDistance("0");
                                                    districtList.setTime("0");
                                                }
                                            }

                                        }
                                        districtsList.add(districtList);
                                    }

                                }

                                districtList = new DistrictList();
                                districtList.setLatitude(data.getJSONObject("data_to").getString("latitude"));
                                districtList.setLongitude(data.getJSONObject("data_to").getString("longitude"));
                                districtList.setName(data.getJSONObject("data_to").getString("name"));
                                districtList.setOrder_id(data.getString("id"));
                            if (data.get("information") instanceof JSONObject) {
                                JSONObject information = data.getJSONObject("information").getJSONObject("exists");
                                if (information.has("data_to")) {
                                    JSONObject stats_value = information.getJSONObject("data_to");
                                    if (stats_value.has("stats")) {
                                        JSONObject stats = stats_value.getJSONObject("stats");
                                        districtList.setDistance(stats.getString("distance"));
                                        districtList.setTime(stats.getString("time"));
                                    }
                                    else {
                                        districtList.setDistance("0");
                                        districtList.setTime("0");
                                    }
                                }
                            }
                                districtsList.add(districtList);
                                itemController.add(i+1);
                                itemType.add(new DetailTripModel(DetailTripModel.DISTRICT_TO_TYPE));
                            }
                                recyclerView.getAdapter().notifyDataSetChanged();

                            } catch(JSONException e){
                                e.printStackTrace();
                            }

                    }
                    @Override
                    public void onError(ANError anError) {
                        Log.i(TAG, anError.getErrorBody().toString());
                        Log.i(TAG, anError.getErrorDetail().toString());
                        Log.i(TAG, String.valueOf(anError.getErrorCode()));
                        Log.i(TAG, anError.getResponse().toString());
                        Toast.makeText(getActivity(), getString(R.string.app_name), Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
