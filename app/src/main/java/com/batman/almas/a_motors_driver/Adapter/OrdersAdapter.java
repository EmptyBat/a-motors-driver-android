package com.batman.almas.a_motors_driver.Adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.batman.almas.a_motors_driver.Model.OrdersModel;
import com.batman.almas.a_motors_driver.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.ViewHolder> {

    private List<OrdersModel> posts;
    private PostsAdapterListener listener;

    public OrdersAdapter(List<OrdersModel> posts, PostsAdapterListener listener) {
        this.listener = listener;
        this.posts = posts;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        OrdersModel post = posts.get(position);
        holder.name_tv.setText(post.getPassanger_name());
        holder.district_tv.setText(post.getData_from_name());
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        SimpleDateFormat global_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat format_time = new SimpleDateFormat("HH:mm");
        try {
            Date date = format.parse(post.getOrder_at());
            Date date_time = global_format.parse(post.getOrder_at());
            holder.date_tv.setText(format.format(date) + " - ");
            holder.hour_tv.setText(format_time.format(date_time));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (post.getComment().equals("null")) {
            holder.comment_tv.setVisibility(View.INVISIBLE);
        }
        holder.comment_tv.setText(post.getComment());
        if (post.getPreliminary() == false) {
            holder.preliminary_tv.setVisibility(View.GONE);
        } else {
            holder.preliminary_tv.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return posts.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView name_tv;
        TextView district_tv;
        TextView date_tv;
        TextView hour_tv;
        TextView comment_tv;
        TextView preliminary_tv;

        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // send selected contact in callback
                    listener.onModelDataselected(posts.get(getAdapterPosition()));
                }
            });
            name_tv = (TextView) itemView.findViewById(R.id.name_tv);
            district_tv = (TextView) itemView.findViewById(R.id.district_tv);
            date_tv = (TextView) itemView.findViewById(R.id.end_time_tv);
            hour_tv = (TextView) itemView.findViewById(R.id.hour_tv);
            comment_tv = (TextView) itemView.findViewById(R.id.comment_tv);
            preliminary_tv = (TextView) itemView.findViewById(R.id.preliminary_tv);
        }
    }
    public interface PostsAdapterListener {
        void onModelDataselected(OrdersModel data);
    }
}