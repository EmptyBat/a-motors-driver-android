package com.batman.almas.a_motors_driver;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.batman.almas.a_motors_driver.Adapter.DistrictAdapter;
import com.batman.almas.a_motors_driver.Fragment.headerFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import fr.quentinklein.slt.LocationTracker;
import fr.quentinklein.slt.TrackerSettings;

import com.batman.almas.a_motors_driver.Model.DistrictList;
import com.batman.almas.a_motors_driver.Model.DistrictModel;
import com.batman.almas.a_motors_driver.Vendor.BackgroundModeGPS;
import com.batman.almas.a_motors_driver.Vendor.Const;
import com.pusher.client.AuthorizationFailureException;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.PrivateChannel;
import com.pusher.client.channel.PrivateChannelEventListener;
import com.pusher.client.connection.ConnectionEventListener;
import com.pusher.client.connection.ConnectionState;
import com.pusher.client.connection.ConnectionStateChange;
import com.pusher.client.util.HttpAuthorizer;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import static com.batman.almas.a_motors_driver.Vendor.Const.BASE_URL;

public class MainActivity  extends AppCompatActivity  implements  NavigationView.OnNavigationItemSelectedListener,PrivateChannelEventListener ,DistrictAdapter.DistrictAdapterListener,LocationListener {
    public static final String TAG = LoginActivity.class.getSimpleName();
    @BindView(R.id.map)
    WebView mapView;

    @BindView(R.id.main_rv)
    RecyclerView recyclerView;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    @BindView(R.id.menu_btn_main)
    Button toogle_btn;

    @BindView(R.id.ready_btn)
    Button main_btn;

    @BindView(R.id.order_timer_status_tv)
    TextView timer_status;

    @BindView(R.id.order_status_tv)
    TextView order_status;

    @BindView(R.id.pause_btn)
    Button pause_btn;

    @BindView(R.id.homeProgressView)
    ProgressBar progressBar;

    LocationManager mLocationManager;
    Pusher pusher;
    PrivateChannel channel;
    JSONObject jsonLocation;
    List<Integer> colorList;
    List<DistrictModel> orderType;
    List<DistrictList> districtsList;
    List<DistrictModel> dropedList;
    List<DistrictModel> orderList;
    DistrictAdapter adapter;
    private String data_from = "data_from";
    private String data_to = "data_from";
    private boolean network = false;
    SharedPreferences prefs;
    String order_id;
    private String data_casual;
    private Timer drive_to;
    private Timer driver_left;
    private Timer waitingTimer;
    private Timer status_id_t;
    private Timer status_updater;
    private int trip_status;
    private int additional_count = 0;
    private int additional_status = 0;
    private boolean hide_data_to = false;
    private boolean wait_or_play = false;
    private int seconds;
    private int minutes;
    private int data_casual_count;
    private boolean driveIsBusy = false;
    private boolean droped = false;
    private boolean isSubscribed = false;
    private  Location driver_location;
    LocationTracker tracker;
    int status_id = 2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidNetworking.initialize(getApplicationContext());
        setContentView(R.layout.activity_drawer);
        ButterKnife.bind(this);
        if (drive_to != null) {
            drive_to.cancel();
            drive_to = null;
        }
        if (driver_left != null) {
            driver_left.cancel();
            drive_to = null;
        }
        if (waitingTimer != null) {
            waitingTimer.cancel();
            waitingTimer = null;
        }
        prefs = getSharedPreferences(Preferences.shPref, 0);
        driver_location = getLocation();
        TrackerSettings settings =
                new TrackerSettings()
                        .setUseGPS(true)
                        .setUseNetwork(true)
                        .setUsePassive(true)
                        .setTimeBetweenUpdates(Integer.valueOf(prefs.getString(Preferences.frequency_without_order, null)))
                        .setMetersBetweenUpdates(Integer.valueOf(prefs.getString(Preferences.frequency_by_distance, null)));
        tracker = new LocationTracker(this, settings) {
            @Override
            public void onTimeout() {
                Log.d("LocationTracker", "onTimeout");
            }
            @Override
            public void onLocationFound(Location location) {
                driver_location = location;
                sendLocationToPusher();
                Log.d("LocationTracker", "onLocationFound");
            }
        };
        stopService(new Intent(this,BackgroundModeGPS.class));

        tracker.startListening();
        createPusher();
        setNavigationDrawer();
        handleData();
        progressBar.setVisibility(View.GONE);
    }
    @Override
    public void onLocationChanged(Location location)
    {

        sendLocationToPusher();
    }
    @Override
    public void onProviderDisabled(String provider)
    {
        Log.e(TAG, "onProviderDisabled: " + provider);
    }
    @Override
    public void onProviderEnabled(String provider)
    {
        Log.e(TAG, "onProviderEnabled: " + provider);
    }
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras)
    {
        Log.e(TAG, "onStatusChanged: " + provider);
    }
    private void handleData() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        driveIsBusy = prefs.getBoolean("driveIsBusy", false);
          if (driveIsBusy == true) {
              status_id = 4;
              if (getIntent().hasExtra("STATUS_ID")) {
                  String status_id = getIntent().getStringExtra("STATUS_ID");
                  String order_id = getIntent().getStringExtra("ORDER_ID");
                  this.order_id = order_id;
                  if (status_id.equals("3")) {
                      ResetOrderStatus();
                  }
              }else {
                  RestoreStatus();
              }
              getDriverStatus();
              status_updater = new Timer();
              status_updater.schedule(new TimerTask() {
                  @Override
                  public void run() {
                      DriverStatusChanged(String.valueOf(status_id));
                  }
              }, 0, 60000);
          }
          else {
              status_id = 2;
              String order_id = getIntent().getStringExtra("ORDER_ID");
              this.order_id = order_id;
              getOrderInfo();
              loadMap("start");
          }
    }
    private void sendLocationToPusher(){
        if (pusher.getConnection().getState() == ConnectionState.CONNECTED) {
            if (channel.isSubscribed()) {
                if (driver_location.getLongitude() != 0) {
                    jsonLocation = new JSONObject();
                    JSONObject location = new JSONObject();
                    try {
                        location.put("longitude", driver_location.getLongitude());
                        location.put("latitude", driver_location.getLatitude());
                        jsonLocation.put("location", location);
                        jsonLocation.put("driver_id", prefs.getString(Preferences.user_id, null));
                        jsonLocation.put("status_id", status_id);
                        jsonLocation.put("car_number", prefs.getString(Preferences.car_id, null));
                        channel.trigger("client-location-updated", jsonLocation.toString());

                    } catch (JSONException e) {
                    }
                }
            }
        }
    }
    private void ResetOrderStatus(){
        trip_status = 1;
        data_from = "data_from";
        data_to = "data_from";
        data_casual = "";
        additional_count =  0;
        additional_status = 0;
        hide_data_to = true;
        if (drive_to != null) {
            drive_to.cancel();
            drive_to = null;
        }
        if (driver_left != null) {
            driver_left.cancel();
            drive_to = null;
        }
        if (waitingTimer != null) {
            waitingTimer.cancel();
            waitingTimer = null;
        }
        getOrderInfo();
    }
    private void createPusher() {
        HashMap<String, String> authHeader = new HashMap<>();
        authHeader.put("Authorization", "Basic ZGV2QGFtb3RvcnMua3o6MTIzMTIz");
        final com.batman.almas.a_motors_driver.HttpAuthorizer authorizer = new  com.batman.almas.a_motors_driver.HttpAuthorizer(
                Const.PUSHER_AUTH_URL );
        authorizer.setHeaders(authHeader);
        PusherOptions options = new PusherOptions().setCluster("ap2").setAuthorizer(authorizer).setEncrypted(true);
        pusher = new Pusher("7ea5886145e8af112d5f", options);
        pusher.connect();
        channel = pusher.subscribePrivate("private-driver-location", new PrivateChannelEventListener() {
            @Override
            public void onSubscriptionSucceeded(String s) {
                jsonLocation = new JSONObject();
                JSONObject location = new JSONObject();
                try {
                    if (driver_location.getLongitude() !=0) {
                        location.put("longitude", driver_location.getLongitude());
                        location.put("latitude", driver_location.getLatitude());
                        jsonLocation.put("location", location);
                        jsonLocation.put("driver_id", prefs.getString(Preferences.user_id, null));
                        jsonLocation.put("status_id", status_id);
                        jsonLocation.put("car_number", prefs.getString(Preferences.car_id, null));
                        channel.trigger("client-location-updated", jsonLocation.toString());
                    }
                    } catch (JSONException e) {

                    }
                    }
                    @Override
                        public void onAuthenticationFailure(String s, Exception e) {
                            Log.d(TAG, "Failure "+ s + e.getLocalizedMessage());
                        }
                        @Override
                        public void onEvent(String s, String s1, String s2) {
                            Log.d(TAG, "fall ");
                        }
                    });
                    Log.d(TAG, "createPusher: " + channel);
    }
    private void RestoreStatus() {
        status_id = 4;
        status_id_t = new Timer();
        status_id_t.schedule(new TimerTask() {
            @Override
            public void run() {
                getDriverStatus();
            }
        }, 0, 9000);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        trip_status = prefs.getInt("trip_status", 0);
        data_to = prefs.getString("data_to", null);
        data_from = prefs.getString("data_from", null);
        data_casual = prefs.getString("data_casual", null);
        driveIsBusy = prefs.getBoolean("driveIsBusy", false);
        order_id = prefs.getString("order_id", null);
        additional_count = prefs.getInt("additional_count", 0);
        additional_status = prefs.getInt("additional_status", 0);
        hide_data_to = true;
        RestoreOrderInfo();
    }
    private void RestoreOrderInfo(){
        String formatString = Const.BASE_URL + "orders/%s?with[]=company";
        String url = String.format(formatString,order_id);
        AndroidNetworking.get(url)
                .addHeaders("Authorization","Basic ZGV2QGFtb3RvcnMua3o6MTIzMTIz")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject data = response.getJSONObject("data");
                            DistrictModel DistrictItemModel = new DistrictModel();
                            DistrictList districtList = new DistrictList();
                            orderType = new ArrayList<>();
                            orderList = new ArrayList<>();
                            districtsList = new ArrayList<>();
                            colorList = new ArrayList<>();
                            dropedList = new ArrayList<>();
                            DistrictItemModel.setCompany_name(data.getJSONObject("company").getString("title"));
                            DistrictItemModel.setPassanger_name(data.getString("passanger_name"));
                            DistrictItemModel.setOrder_ate(data.getString("order_at"));
                            orderList.add(DistrictItemModel);
                            orderType.add(new DistrictModel(DistrictModel.NAME_TYPE));
                            orderType.add(new DistrictModel(DistrictModel.ADDITIONA_DISTRICT_TYPE));
                            dropedList.add(new DistrictModel(DistrictModel.NAME_TYPE));
                            dropedList.add(new DistrictModel(DistrictModel.ADDITIONA_DISTRICT_TYPE));
                            colorList.add(R.color.whiteColor);
                            districtList.setLatitude(data.getJSONObject("data_from").getString("latitude"));
                            districtList.setLongitude(data.getJSONObject("data_from").getString("longitude"));
                            districtList.setName(data.getJSONObject("data_from").getString("name"));
                            districtsList.add(districtList);
                            if (data.get("data_additional") instanceof JSONArray)
                            {
                                JSONArray data_additional = data.optJSONArray("data_additional");
                                for (int j = 0; j < data_additional.length(); j++) {
                                    districtList = new DistrictList();
                                    JSONObject jsonObject = data_additional.getJSONObject(j);
                                    districtList.setName(jsonObject.getString("name"));
                                    districtList.setLongitude(jsonObject.getString("longitude"));
                                    districtList.setLatitude(jsonObject.getString("latitude"));
                                    orderType.add(new DistrictModel(DistrictModel.ADDITIONA_DISTRICT_TYPE));
                                    districtsList.add(districtList);
                                    colorList.add(R.color.LightGrayColor);
                                }
                            }
                            districtList = new DistrictList();
                            districtList.setLatitude(data.getJSONObject("data_to").getString("latitude"));
                            districtList.setLongitude(data.getJSONObject("data_to").getString("longitude"));
                            districtList.setName(data.getJSONObject("data_to").getString("name"));
                            districtsList.add(districtList);
                            colorList.add(R.color.whiteColor);
                            orderType.add(new DistrictModel(DistrictModel.DISTRICT_TO_TYPE));
                            dropedList.add(new DistrictModel(DistrictModel.DISTRICT_TO_TYPE));
                            hide_data_to  = true;
                            adapter = new DistrictAdapter(orderList,orderType,colorList,districtsList,MainActivity.this,hide_data_to,MainActivity.this);
                            LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
                            llm.setOrientation(LinearLayoutManager.VERTICAL);
                            recyclerView.setLayoutManager(llm);
                            recyclerView.setAdapter(adapter);
                            setItem();
                            GetStatusId();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        Log.i(TAG, anError.getErrorBody().toString());
                        Log.i(TAG, anError.getErrorDetail().toString());
                        Log.i(TAG, String.valueOf(anError.getErrorCode()));
                        Log.i(TAG, anError.getResponse().toString());
                        Toast.makeText(MainActivity.this, getString(R.string.bad_internet_connection), Toast.LENGTH_SHORT).show();
                    }
                });
    }
    private void GetStatusId(){
        if (network == true) {
            switch (trip_status) {
                case 0:
                    break;
                case 1:
                    order_status.setText("Клиент ожидает машину");
                    trip_status = 1;
                    data_from = "data_from";
                    loadMap("drive");
                    main_btn.setText("На месте");
                    driveIsBusy = true;
                    driver_left = new Timer();
                    driver_left.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            driverLeft();
                        }

                    }, 0, 5000);
                    SaveStatus();
                    break;
                case 2:
                    order_status.setText("Ожидаем пассажира");
                    trip_status = 2;
                    data_from = "data_from";
                    main_btn.setText("Пассажир на месте");
                    loadMap("drive");
                    driverArrived();
                    SaveStatus();
                    break;
                case 3:
                    order_status.setText("Едем к месту назначения");
                    RestoreNextPoint();
                    break;
                case 4:
                    order_status.setText("Едем к месту назначения");
                    data_to = "data_to";
                    drive_to = new Timer();
                    drive_to.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            driveTo();
                        }
                    }, 0, 5000);
                    main_btn.setText("Завершить поездку");
                    trip_status = 4;
                    colorList.set(additional_status - 1, R.color.LightGrayColor);
                    colorList.set(additional_status, R.color.whiteColor);
                    adapter.notifyDataSetChanged();
                    loadMap("drive");
                    break;
            }
        }
        else {
            Toast.makeText(MainActivity.this, "Плохое интернет соединение", Toast.LENGTH_SHORT).show();
        }
    }
    private void RestoreNextPoint(){
        data_from = "data_from";
        drive_to = new Timer();
        drive_to.schedule(new TimerTask() {
            @Override
            public void run() {
                driveTo();
            }
        }, 0, 5000);
            trip_status = 3;
            data_from = data_to;
            data_to = "data_additional_" + (additional_status);
            main_btn.setText("Приехали к месту #" + (additional_status));
            colorList.set(additional_status-1,R.color.LightGrayColor);
            colorList.set(additional_status,R.color.whiteColor);
            adapter.notifyDataSetChanged();
        loadMap("drive");
        SaveStatus();
    }
    private void SaveStatus() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("trip_status",trip_status);
        editor.putString("data_to",data_to);
        editor.putString("data_from",data_from);
        editor.putString("data_casual",data_casual);
        editor.putBoolean("driveIsBusy",driveIsBusy);
        editor.putString("order_id",order_id);
        editor.putInt("additional_count",additional_count);
        editor.putInt("additional_status",additional_status);
        editor.putInt("driver_status",status_id);
        editor.commit();
    }
    private boolean checkLocationExternalPermission()
    {
        String permission = android.Manifest.permission.ACCESS_FINE_LOCATION;
        int res = getApplicationContext().checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }
    private void loadMap(String status) {
        String formatString;
        String url;
        if (status == "start") {
            formatString = BASE_URL + "map-driver?method=start&order_id=%s&driver_id=%s&latitude=%s&longitude=%s";
            url = String.format(formatString, order_id, prefs.getString(Preferences.user_id, null), driver_location.getLatitude(), driver_location.getLongitude());
        } else {
            formatString = BASE_URL + "map-driver?method=drive&order_id=%s&driver_id=%s&from=%s&to=%s";
            url = String.format(formatString, order_id, prefs.getString(Preferences.user_id, null), data_from, data_to);
        }
        WebSettings webSettings = mapView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        mapView.loadUrl(url);
        mapView.setWebViewClient(new WebViewClient());
        if (pusher.getConnection().getState() == ConnectionState.CONNECTED) {
            if (channel.isSubscribed()) {
                jsonLocation = new JSONObject();
                JSONObject location_object = new JSONObject();
                try {
                    if (driver_location.getLongitude() !=0) {
                        location_object.put("longitude", driver_location.getLongitude());
                        location_object.put("latitude", driver_location.getLatitude());
                        jsonLocation.put("location", location_object);
                        jsonLocation.put("driver_id", prefs.getString(Preferences.user_id, null));
                        jsonLocation.put("status_id", status_id);
                        jsonLocation.put("car_number", prefs.getString(Preferences.car_id, null));
                        channel.trigger("client-location-updated", jsonLocation.toString());
                    }
                } catch (JSONException e) {
                }
            }
        }
    }

    @Override
    public void onEvent(final String channelName, final String eventName, final String data) {

        System.out.println(String.format("Received event [%s] on channel [%s] with data [%s]", eventName, channelName,
                data));
    }

    @Override
    public void onSubscriptionSucceeded(final String channelName) {
        isSubscribed = true;
        System.out.println(String.format("Subscription to channel [%s] succeeded", channel.getName()));
    }

    @Override
    public void onAuthenticationFailure(final String message, final Exception e) {

        System.out.println(String.format("Authentication failure due to [%s], exception was [%s]", message, e));
    }
    private static final String[] INITIAL_PERMS={
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.READ_CONTACTS
    };
    private static final String[] LOCATION_PERMS={
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private void setNavigationDrawer() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, null, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_force_remove).setVisible(false);
        View headerView = navigationView.getHeaderView(0);
        TextView navUsername = (TextView) headerView.findViewById(R.id.name_tv);
        TextView navPhone = (TextView) headerView.findViewById(R.id.phone_tv);
        CircleImageView navImage = (CircleImageView) headerView.findViewById(R.id.profile_image);
        navUsername.setText(prefs.getString(Preferences.userName, null));
        navPhone.setText(prefs.getString(Preferences.userPhone, null));
        Picasso.get()
                .load(Const.DOMAIN_URL  +prefs.getString(Preferences.image_url, null) )
                .into(navImage);
        drawer.closeDrawer(Gravity.LEFT);
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

           finish();
        }
    }
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_history) {
            Intent intent = new Intent(getBaseContext(), HistoryActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_dispatcher) {

        } else if (id == R.id.nav_exit) {
            SharedPreferences sharedPreferences = getSharedPreferences(Preferences.shPref, 0);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(Preferences.user_id, null);
            editor.commit();
            Intent newAct = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(newAct);
            finish();

        }
          else if (id == R.id.nav_force_remove) {
            if (data_from == "data_additional_1" ||  data_from == "data_additional_2"  ||  data_from == "data_additional_3"
                    ||  data_from == "data_additional_4"
                    ||  data_from == "data_additional_5") {
                force_finish();
            }
            else {

            }
    }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    @OnClick(R.id.menu_btn_main)
    public void toogle_btn(){
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.openDrawer(GravityCompat.START);
    }
    private Location getLocation(){
        mLocationManager = (LocationManager)getApplicationContext().getSystemService(LOCATION_SERVICE);
        List<String> providers = mLocationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            Location l = mLocationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }
        return bestLocation;
    }
    private void order_acceptedUI(){
        order_status.setText("Клиент ожидает машину");
        trip_status = 1;
        data_from = "data_from";
        data_to = data_to;
        loadMap("drive");
        main_btn.setText("На месте");
        acceptOrder();
        hide_data_to = true;
        driveIsBusy = true;
        adapter = new DistrictAdapter(orderList,orderType,colorList,districtsList,MainActivity.this,hide_data_to,this);
        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(adapter);
        SaveStatus();
    }
    private void driverArrivedUI(){
        order_status.setText("Ожидаем пассажира");
        trip_status = 2;
        driver_left.cancel();
        driver_left = null;
        data_from = "data_from";
        main_btn.setText("Пассажир на месте");
        driverArrived();
        SaveStatus();
    }
    private void passangerPickedUI(){
        order_status.setText("Едем к месту назначения");
        trip_status = 3;
        passangerPickerConfirm();
        passangerPicked();
        SaveStatus();

    }
    private void arrivedtoUI(){
    }
    private void passangerPickerConfirm(){
        data_from = "data_from";
        if (additional_count == 0) {
            data_to = "data_to";
            main_btn.setText("Завершить поездку");
            trip_status = 4;
            colorList.set(additional_status,R.color.LightGrayColor);
            colorList.set(additional_status+1,R.color.whiteColor);
            adapter.notifyDataSetChanged();
        }
        else {
            trip_status = 3;
            additional_count --;
            data_from = data_to;
            data_to = "data_additional_" + (additional_status+1);
            Log.d("asffsaafs","afsfsafs" +additional_status + "----" + data_to);
            main_btn.setText("Приехали к месту #" + (additional_status+1));
            colorList.set(additional_status,R.color.LightGrayColor);
            colorList.set(additional_status+1,R.color.whiteColor);
            adapter.notifyDataSetChanged();
        }
        additional_status++;
        loadMap("drive");
        Log.d("passanger-picked","test" + additional_status + " "+ data_to + " " + data_from );
    }
    private void nextPoint(){
        if (driver_left !=null) {
            driver_left.cancel();
            driver_left = null;
        }
        if (drive_to !=null) {
            drive_to.cancel();
            drive_to = null;
        }
        data_from = "data_from";
        if (additional_count == 0) {
            if (districtsList.size() > 2) {
                Map<String, String> params = new HashMap<>();
                params.put("driver_id", prefs.getString(Preferences.user_id, null));
                params.put("longitude", Double.toString(driver_location.getLongitude()));
                params.put("latitude",  Double.toString(driver_location.getLatitude()));
                params.put("data_key",  "data_additional_" + (additional_status));
                Log.d("districtsList","districtsList" + additional_status);
                AndroidNetworking.post(Const.ORDER_URL + order_id + "/arrived-to" )
                        .addBodyParameter(params)
                        .addHeaders("Authorization","Basic ZGV2QGFtb3RvcnMua3o6MTIzMTIz")
                        .setPriority(Priority.MEDIUM)
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    drive_to = new Timer();
                                    drive_to.schedule(new TimerTask() {
                                        @Override
                                        public void run() {
                                            driveTo();
                                        }
                                    }, 0, 5000);
                                    JSONObject data = response.getJSONObject("data");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            @Override
                            public void onError(ANError anError) {
                                Log.i(TAG, anError.getErrorBody().toString());
                                Log.i(TAG, anError.getErrorDetail().toString());
                                Log.i(TAG, String.valueOf(anError.getErrorCode()));
                                Log.i(TAG, anError.getResponse().toString());
                                Toast.makeText(MainActivity.this, getString(R.string.bad_internet_connection), Toast.LENGTH_SHORT).show();
                            }
                        });
                Log.d("fasfas","afsfas" + districtsList.size());
            }
            data_to = "data_to";
            main_btn.setText("Завершить поездку");
            trip_status = 4;
            colorList.set(additional_status,R.color.LightGrayColor);
            colorList.set(additional_status+1,R.color.whiteColor);
            adapter.notifyDataSetChanged();
        }
        else {
            trip_status = 3;
            additional_count --;
            data_from = data_to;
            data_to = "data_additional_" + (additional_status);
            main_btn.setText("Приехали к месту #" + (additional_status+1));
            colorList.set(additional_status,R.color.LightGrayColor);
            colorList.set(additional_status+1,R.color.whiteColor);
            adapter.notifyDataSetChanged();
            Map<String, String> params = new HashMap<>();
            params.put("driver_id", prefs.getString(Preferences.user_id, null));
            params.put("longitude", Double.toString(driver_location.getLongitude()));
            params.put("latitude",  Double.toString(driver_location.getLatitude()));
            params.put("data_key",  "data_additional_" + (additional_status));
            Log.d("districtsList","districtsList" + additional_status);
            AndroidNetworking.post(Const.ORDER_URL + order_id + "/arrived-to" )
                    .addBodyParameter(params)
                    .addHeaders("Authorization","Basic ZGV2QGFtb3RvcnMua3o6MTIzMTIz")
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                data_to = "data_additional_" + (additional_status);
                                drive_to = new Timer();
                                drive_to.schedule(new TimerTask() {
                                    @Override
                                    public void run() {
                                        driveTo();
                                    }
                                }, 0, 5000);
                                JSONObject data = response.getJSONObject("data");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        @Override
                        public void onError(ANError anError) {
                            Log.i(TAG, anError.getErrorBody().toString());
                            Log.i(TAG, anError.getErrorDetail().toString());
                            Log.i(TAG, String.valueOf(anError.getErrorCode()));
                            Log.i(TAG, anError.getResponse().toString());
                            Toast.makeText(MainActivity.this, getString(R.string.bad_internet_connection), Toast.LENGTH_SHORT).show();
                        }
                    });
        }
        additional_status++;
        Log.d("fasfas","afsfas" + additional_status);
        loadMap("drive");
        SaveStatus();
    }
    private void complete_order() {
        if (wait_or_play == false){
        SharedPreferences prefs = getSharedPreferences(Preferences.shPref, 0);
        if (prefs.getBoolean(Preferences.arrivedTo, false) == false) {
            arrivedTo();
            SharedPreferences shared = getSharedPreferences(Preferences.shPref, 0);
            SharedPreferences.Editor edit = shared.edit();
            edit.putBoolean(Preferences.arrivedTo,true);
            edit.commit();
        }
        if (drive_to != null){
            drive_to.cancel();
            drive_to = null;
        }
        pusher.disconnect();
        progressBar.setVisibility(View.VISIBLE);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.GONE);
                Intent intent = new Intent(getBaseContext(), TripDetailActivity.class);
                intent.putExtra("ORDER_ID", order_id);
                startActivity(intent);
            }
        }, 5000);
        }
        else {
            Toast.makeText(MainActivity.this, getString(R.string.disable_timer), Toast.LENGTH_SHORT).show();
        }
    }
    @OnClick(R.id.ready_btn)
    public void main_btn(){
        switch (trip_status){
            case 0:
                order_acceptedUI();
                break;
            case 1:
                driverArrivedUI();
                break;
            case 2:
                passangerPickedUI();
                break;
            case 3:
                nextPoint();
                break;
            case 4:
                complete_order();
                break;
        }
    }
    @OnClick(R.id.pause_btn)
    public void pauseClicked(){
        if (wait_or_play == false){
         passangerLeftUI();
     }
     else {
         passangerBackUI();
     }
    }
    private void passangerLeftUI(){
        data_casual_count +=1;
        data_casual = "data_casual_" + String.valueOf(data_casual_count);
        pause_btn.setBackgroundResource(R.drawable.play_icon);
        order_status.setText("Ожидание");
        passangerLeft();
        wait_or_play = true;
        waitingTimer = new Timer();
        waitingTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                timer();
            }
        }, 0, 1000);
    }
    private void passangerBackUI(){
        pause_btn.setBackgroundResource(R.drawable.pause_icon);
        order_status.setText("Едем к месту назначения");
        timer_status.setText("Пауза");
        minutes = 0;
        seconds = 0;
        passangerBack();
        waitingTimer.cancel();
        wait_or_play = false;
    }
    private void timer(){
        if (seconds == 59){
            minutes ++;
            seconds = 1;
        }
        else {
            seconds ++;
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                timer_status.setText(String.valueOf(minutes) + ":" + String.valueOf(seconds));
            }
        });
    }
    private void setItem(){
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, null, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);navigationView.setNavigationItemSelectedListener(this);
        Menu nav_Menu = navigationView.getMenu();
        if (districtsList.size() >=3) {
            nav_Menu.findItem(R.id.nav_force_remove).setVisible(true);
        }
        View headerView = navigationView.getHeaderView(0);
        TextView navUsername = (TextView) headerView.findViewById(R.id.name_tv);
        TextView navPhone = (TextView) headerView.findViewById(R.id.phone_tv);
        CircleImageView navImage = (CircleImageView) headerView.findViewById(R.id.profile_image);
        navUsername.setText(prefs.getString(Preferences.userName, null));
        navPhone.setText(prefs.getString(Preferences.userPhone, null));
        Picasso.get()
                .load(Const.DOMAIN_URL  +prefs.getString(Preferences.image_url, null) )
                .into(navImage);
        drawer.closeDrawer(Gravity.LEFT);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////
    private void getOrderInfo(){
        String formatString = Const.BASE_URL + "orders/%s?with[]=company";
        String url = String.format(formatString,order_id);
        AndroidNetworking.get(url)
                .addHeaders("Authorization","Basic ZGV2QGFtb3RvcnMua3o6MTIzMTIz")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject data = response.getJSONObject("data");
                            DistrictModel DistrictItemModel = new DistrictModel();
                            DistrictList districtList = new DistrictList();
                            orderType = new ArrayList<>();
                            orderList = new ArrayList<>();
                            districtsList = new ArrayList<>();
                            colorList = new ArrayList<>();
                            dropedList = new ArrayList<>();
                            DistrictItemModel.setCompany_name(data.getJSONObject("company").getString("title"));
                            DistrictItemModel.setPassanger_name(data.getString("passanger_name"));
                            DistrictItemModel.setOrder_ate(data.getString("order_at"));
                            orderList.add(DistrictItemModel);
                            orderType.add(new DistrictModel(DistrictModel.NAME_TYPE));
                            orderType.add(new DistrictModel(DistrictModel.ADDITIONA_DISTRICT_TYPE));
                            dropedList.add(new DistrictModel(DistrictModel.NAME_TYPE));
                            dropedList.add(new DistrictModel(DistrictModel.ADDITIONA_DISTRICT_TYPE));
                            colorList.add(R.color.whiteColor);
                            districtList.setLatitude(data.getJSONObject("data_from").getString("latitude"));
                            districtList.setLongitude(data.getJSONObject("data_from").getString("longitude"));
                            districtList.setName(data.getJSONObject("data_from").getString("name"));
                            districtsList.add(districtList);
                            if (data.get("data_additional") instanceof JSONArray)
                            {
                                JSONArray data_additional = data.optJSONArray("data_additional");
                                for (int j = 0; j < data_additional.length(); j++) {
                                    districtList = new DistrictList();
                                    JSONObject jsonObject = data_additional.getJSONObject(j);
                                    districtList.setName(jsonObject.getString("name"));
                                    districtList.setLongitude(jsonObject.getString("longitude"));
                                    districtList.setLatitude(jsonObject.getString("latitude"));
                                    colorList.add(R.color.LightGrayColor);
                                    orderType.add(new DistrictModel(DistrictModel.ADDITIONA_DISTRICT_TYPE));
                                    districtsList.add(districtList);
                                    additional_count ++;
                                }
                            }
                            districtList = new DistrictList();
                            districtList.setLatitude(data.getJSONObject("data_to").getString("latitude"));
                            districtList.setLongitude(data.getJSONObject("data_to").getString("longitude"));
                            districtList.setName(data.getJSONObject("data_to").getString("name"));
                            districtsList.add(districtList);
                            colorList.add(R.color.LightGrayColor);
                            orderType.add(new DistrictModel(DistrictModel.DISTRICT_TO_TYPE));
                            dropedList.add(new DistrictModel(DistrictModel.DISTRICT_TO_TYPE));
                            adapter = new DistrictAdapter(orderList,orderType,colorList,districtsList,MainActivity.this,hide_data_to,MainActivity.this);
                            LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
                            llm.setOrientation(LinearLayoutManager.VERTICAL);
                            recyclerView.setLayoutManager(llm);
                            recyclerView.setAdapter(adapter);
                            setItem();
                        } catch (JSONException e) {
                            e.printStackTrace();
                    }
                        String driver_id = getIntent().getStringExtra("DRIVER_ID");
                        if (prefs.getString(Preferences.user_id, null).equals(driver_id)) {
                            order_acceptedUI();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        Log.i(TAG, anError.getErrorBody().toString());
                        Log.i(TAG, anError.getErrorDetail().toString());
                        Log.i(TAG, String.valueOf(anError.getErrorCode()));
                        Log.i(TAG, anError.getResponse().toString());
                        Toast.makeText(MainActivity.this, getString(R.string.bad_internet_connection), Toast.LENGTH_SHORT).show();
                    }
                });
    }
    private void acceptOrder() {
        Map<String, String> params = new HashMap<>();
        params.put("driver_id",  prefs.getString(Preferences.user_id, null));
        AndroidNetworking.post(Const.ORDER_URL + order_id + "/accept" )
                .addBodyParameter(params)
                .addHeaders("Authorization","Basic ZGV2QGFtb3RvcnMua3o6MTIzMTIz")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        status_id = 4;
                        Log.d("sffs","working" + response.toString());
                        try {
                            if (response.has("code")) {
                                String code = response.getString("code");
                                if (code.equals("302")) {
                                    Toast.makeText(MainActivity.this, getString(R.string.unfortune), Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(getBaseContext(), OrdersActivity.class);
                                    startActivity(intent); }
                            } else {
                                driver_left = new Timer();
                                driver_left.schedule(new TimerTask() {
                                    @Override
                                    public void run() {
                                        driverLeft();
                                    }

                                }, 0, 5000);
                                status_id_t = new Timer();
                                status_id_t.schedule(new TimerTask() {
                                    @Override
                                    public void run() {
                                        getDriverStatus();
                                    }
                                }, 0, 9000);
                            }

                        } catch (JSONException e) {
                            Log.d("asfasffas", "afsasfa" + "catch working");
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        Log.i(TAG, anError.getErrorBody().toString());
                        Log.i(TAG, anError.getErrorDetail().toString());
                        Log.i(TAG, String.valueOf(anError.getErrorCode()));
                        Log.i(TAG, anError.getResponse().toString());
                        Toast.makeText(MainActivity.this, getString(R.string.bad_internet_connection), Toast.LENGTH_SHORT).show();
                    }
                });
    }
    private void driverLeft() {
        Map<String, String> params = new HashMap<>();
        params.put("driver_id", prefs.getString(Preferences.user_id, null));
        params.put("longitude", Double.toString(driver_location.getLongitude()));
        params.put("latitude",  Double.toString(driver_location.getLatitude()));
        AndroidNetworking.post(Const.ORDER_URL + order_id + "/driver-left" )
                .addBodyParameter(params)
                .addHeaders("Authorization","Basic ZGV2QGFtb3RvcnMua3o6MTIzMTIz")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                    Log.d(TAG,"driver left");
                    }
                    @Override
                    public void onError(ANError anError) {
                        Log.i(TAG, anError.getErrorBody().toString());
                        Log.i(TAG, anError.getErrorDetail().toString());
                        Log.i(TAG, String.valueOf(anError.getErrorCode()));
                        Log.i(TAG, anError.getResponse().toString());
                       // Toast.makeText(MainActivity.this, getString(R.string.bad_internet_connection), Toast.LENGTH_SHORT).show();
                    }
                });
    }
    private void driverArrived() {
        Map<String, String> params = new HashMap<>();
        params.put("driver_id", prefs.getString(Preferences.user_id, null));
        params.put("longitude", Double.toString(driver_location.getLongitude()));
        params.put("latitude",  Double.toString(driver_location.getLatitude()));
        AndroidNetworking.post(Const.ORDER_URL + order_id + "/driver-arrived" )
                .addBodyParameter(params)
                .addHeaders("Authorization","Basic ZGV2QGFtb3RvcnMua3o6MTIzMTIz")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject data = response.getJSONObject("data");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        Log.i(TAG, anError.getErrorBody().toString());
                        Log.i(TAG, anError.getErrorDetail().toString());
                        Log.i(TAG, String.valueOf(anError.getErrorCode()));
                        Log.i(TAG, anError.getResponse().toString());
                        Toast.makeText(MainActivity.this, getString(R.string.bad_internet_connection), Toast.LENGTH_SHORT).show();
                    }
                });
    }
    private void passangerPicked() {
        Map<String, String> params = new HashMap<>();
        params.put("driver_id", prefs.getString(Preferences.user_id, null));
        AndroidNetworking.post(Const.ORDER_URL + order_id + "/passanger-picked")
                .addBodyParameter(params)
                .addHeaders("Authorization", "Basic ZGV2QGFtb3RvcnMua3o6MTIzMTIz")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        drive_to = new Timer();
                        drive_to.schedule(new TimerTask() {
                            @Override
                            public void run() {
                                driveTo();
                            }

                        }, 0, 5000);
                        try {
                            JSONObject data = response.getJSONObject("data");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        Log.i(TAG, anError.getErrorBody().toString());
                        Log.i(TAG, anError.getErrorDetail().toString());
                        Log.i(TAG, String.valueOf(anError.getErrorCode()));
                        Log.i(TAG, anError.getResponse().toString());
                        Toast.makeText(MainActivity.this, getString(R.string.bad_internet_connection), Toast.LENGTH_SHORT).show();
                    }
                });
    }
    private void driveTo() {
        Map<String, String> params = new HashMap<>();
        params.put("driver_id", prefs.getString(Preferences.user_id, null));
        params.put("longitude", Double.toString(driver_location.getLongitude()));
        params.put("latitude",  Double.toString(driver_location.getLatitude()));
        params.put("data_key",  data_to);
        Log.d("asfasfa","driving" + params);
        AndroidNetworking.post(Const.ORDER_URL + order_id + "/drive-to" )
                .addBodyParameter(params)
                .addHeaders("Authorization","Basic ZGV2QGFtb3RvcnMua3o6MTIzMTIz")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG,"driveTo");
                    }
                    @Override
                    public void onError(ANError anError) {
                        Log.i(TAG, anError.getErrorBody().toString());
                        Log.i(TAG, anError.getErrorDetail().toString());
                        Log.i(TAG, String.valueOf(anError.getErrorCode()));
                        Log.i(TAG, anError.getResponse().toString());
                      //  Toast.makeText(MainActivity.this, getString(R.string.bad_internet_connection), Toast.LENGTH_SHORT).show();
                    }
                });
    }
    private void arrivedTo() {
        Map<String, String> params = new HashMap<>();
        params.put("driver_id", prefs.getString(Preferences.user_id, null));
        params.put("longitude", Double.toString(driver_location.getLongitude()));
        params.put("latitude",  Double.toString(driver_location.getLatitude()));
        params.put("data_key",  data_to);
        Log.d("arrivedTO","test" + params);
        AndroidNetworking.post(Const.ORDER_URL + order_id + "/arrived-to" )
                .addBodyParameter(params)
                .addHeaders("Authorization","Basic ZGV2QGFtb3RvcnMua3o6MTIzMTIz")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            drive_to = new Timer();
                            drive_to.schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    driveTo();
                                }

                            }, 0, 5000);
                            JSONObject data = response.getJSONObject("data");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        Log.i(TAG, anError.getErrorBody().toString());
                        Log.i(TAG, anError.getErrorDetail().toString());
                        Log.i(TAG, String.valueOf(anError.getErrorCode()));
                        Log.i(TAG, anError.getResponse().toString());
                        Toast.makeText(MainActivity.this, getString(R.string.bad_internet_connection), Toast.LENGTH_SHORT).show();
                    }
                });
    }
    private void passangerLeft() {
        Map<String, String> params = new HashMap<>();
        params.put("driver_id", prefs.getString(Preferences.user_id, null));
        params.put("data_key",  data_casual);
        params.put("longitude", Double.toString(driver_location.getLongitude()));
        params.put("latitude",  Double.toString(driver_location.getLatitude()));
        AndroidNetworking.post(Const.ORDER_URL + order_id + "/passanger-left")
                .addBodyParameter(params)
                .addHeaders("Authorization","Basic ZGV2QGFtb3RvcnMua3o6MTIzMTIz")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject data = response.getJSONObject("data");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        Log.i(TAG, anError.getErrorBody().toString());
                        Log.i(TAG, anError.getErrorDetail().toString());
                        Log.i(TAG, String.valueOf(anError.getErrorCode()));
                        Log.i(TAG, anError.getResponse().toString());
                        Toast.makeText(MainActivity.this, getString(R.string.bad_internet_connection), Toast.LENGTH_SHORT).show();
                    }
                });
    }
    private void passangerBack() {
        Map<String, String> params = new HashMap<>();
        params.put("driver_id", prefs.getString(Preferences.user_id, null));
        params.put("data_key",  data_casual);
        params.put("longitude", Double.toString(driver_location.getLongitude()));
        params.put("latitude",  Double.toString(driver_location.getLatitude()));
        AndroidNetworking.post(Const.ORDER_URL + order_id + "/passanger-back" )
                .addBodyParameter(params)
                .addHeaders("Authorization","Basic ZGV2QGFtb3RvcnMua3o6MTIzMTIz")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.d("asffsfas","safsfaa"+response);
                            JSONObject data = response.getJSONObject("data");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        Log.i(TAG, anError.getErrorBody().toString());
                        Log.i(TAG, anError.getErrorDetail().toString());
                        Log.i(TAG, String.valueOf(anError.getErrorCode()));
                        Log.i(TAG, anError.getResponse().toString());
                        Toast.makeText(MainActivity.this, getString(R.string.bad_internet_connection), Toast.LENGTH_SHORT).show();
                    }
                });
    }
    public void DriverStatusChanged(String status_id) {
        isOnline();
        Log.d("main_activity","asfasfa" + status_id);
        Map<String, String> params = new HashMap<>();
        params.put("status_id",status_id);
        params.put("latitude",String.valueOf(driver_location.getLatitude()));
        params.put("longitude",String.valueOf(driver_location.getLongitude()));
        AndroidNetworking.post(Const.BASE_URL + "drivers/"+ prefs.getString(Preferences.user_id, null) +"/change_status")
                .addHeaders("Authorization","Basic ZGV2QGFtb3RvcnMua3o6MTIzMTIz")
                .addBodyParameter(params)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) { }
                    @Override
                    public void onError(ANError anError) {
                        Log.i(TAG, anError.getErrorBody().toString());
                        Log.i(TAG, anError.getErrorDetail().toString());
                        Log.i(TAG, String.valueOf(anError.getErrorCode()));
                        Log.i(TAG, anError.getResponse().toString());
                        Log.i(TAG, "HERE2");
                    }
                });
    }
    @OnClick(R.id.doubleGis_btn)
    public void double_gis(){
        Uri uri = Uri.parse("dgis://2gis.ru/routeSearch/rsType/car/to/" + districtsList.get(additional_status).getLongitude()+ "," +districtsList.get(additional_status).getLatitude());

        Intent intent = new Intent(Intent.ACTION_VIEW, uri);

        PackageManager packageManager = getPackageManager();

        List<ResolveInfo> activities = packageManager.queryIntentActivities(intent, 0);

        boolean isIntentSafe = activities.size() > 0;

        if (isIntentSafe) {

            startActivity(intent);

        } else {

            intent = new Intent(Intent.ACTION_VIEW);

            intent.setData(Uri.parse("market://details?id=ru.dublgis.dgismobile"));

            startActivity(intent);
        }
    }
    private void getDriverStatus() {
        AndroidNetworking.get(Const.GET_DRIVER_STATUS + prefs.getString(Preferences.user_id, null))
                .addHeaders("Authorization","Basic ZGV2QGFtb3RvcnMua3o6MTIzMTIz")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        SharedPreferences pref_default = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        boolean driveIsBusy = pref_default.getBoolean("driveIsBusy", false);
                        if (driveIsBusy == true) {
                            try {
                                if (response.getJSONObject("data").getString("status_id").equals("4")){
                                }
                                else {
                                   /* Intent intent = new Intent(getBaseContext(), OrdersActivity.class);
                                    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                                    SharedPreferences.Editor editor = sharedPreferences.edit();
                                    editor.putBoolean("driveIsBusy", false);
                                    editor.commit();
                                    SharedPreferences shared = getSharedPreferences(Preferences.shPref, 0);
                                    SharedPreferences.Editor edit = shared.edit();
                                    edit.putBoolean(Preferences.arrivedTo,false);
                                    edit.commit();
                                    startActivity(intent);
                                    */
                                    status_id_t.cancel();
                                    if (drive_to != null) {
                                        drive_to.cancel();
                                        drive_to = null;
                                    }
                                    if (driver_left != null) {
                                        driver_left.cancel();
                                        drive_to = null;
                                    }
                                    if (waitingTimer != null) {
                                        waitingTimer.cancel();
                                        waitingTimer = null;
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        Log.i(TAG, anError.getErrorBody().toString());
                        Log.i(TAG, anError.getErrorDetail().toString());
                        Log.i(TAG, String.valueOf(anError.getErrorCode()));
                        Log.i(TAG, anError.getResponse().toString());
                       // Toast.makeText(MainActivity.this, getString(R.string.bad_internet_connection), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void force_finish() {
        Map<String, String> params = new HashMap<>();
        params.put("driver_id", prefs.getString(Preferences.user_id, null));
        params.put("data_key",  data_from);
        String formatString = Const.FORCE_FINISH;
        String url = String.format(formatString,order_id);
        AndroidNetworking.post(url)
                .addBodyParameter(params)
                .addHeaders("Authorization","Basic ZGV2QGFtb3RvcnMua3o6MTIzMTIz")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                     complete_order();
                    }
                    @Override
                    public void onError(ANError anError) {
                        Log.i(TAG, anError.getErrorBody().toString());
                        Log.i(TAG, anError.getErrorDetail().toString());
                        Log.i(TAG, String.valueOf(anError.getErrorCode()));
                        Log.i(TAG, anError.getResponse().toString());
                        // Toast.makeText(MainActivity.this, getString(R.string.app_nbad_internet_connectioname), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public void onModelDataselected(int index) {
        if (droped == false) {
            droped = true;
            adapter = new DistrictAdapter(orderList, dropedList, colorList, districtsList, MainActivity.this, hide_data_to, this);
            LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
            llm.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerView.setLayoutManager(llm);
            recyclerView.setAdapter(adapter);
        }
        else {
            droped = false;
            adapter = new DistrictAdapter(orderList, orderType, colorList, districtsList, MainActivity.this, hide_data_to, this);
            LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
            llm.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerView.setLayoutManager(llm);
            recyclerView.setAdapter(adapter);
        }
    }
    @Override
    public void onResume()
    {
        super.onResume();
        stopService(new Intent(this,BackgroundModeGPS .class));
    }
    @Override
    public void onPause()
    {
        super.onPause();
        startService(new Intent(this,BackgroundModeGPS .class));
    }
    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        network = netInfo != null && netInfo.isConnectedOrConnecting();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

}