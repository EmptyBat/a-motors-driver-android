package com.batman.almas.a_motors_driver.Model;

public class StatsModel {
    private String time_drive;
    private String distance;
    private String time_wait;
    public StatsModel() {
    }
    public String getTime_drive() {
        return time_drive;
    }

    public void setTime_drive(String time_drive) {
        this.time_drive = time_drive;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getTime_wait() {
        return time_wait;
    }

    public void setTime_wait(String time_wait) {
        this.time_wait = time_wait;
    }

}
