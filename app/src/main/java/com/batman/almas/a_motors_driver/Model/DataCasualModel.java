package com.batman.almas.a_motors_driver.Model;

import java.io.Serializable;

public class DataCasualModel implements Serializable

    {
        private String waitTime;
        private String title;

    public DataCasualModel() {
    }
        public String getWaitTime() {
        return waitTime;
    }

        public void setWaitTime (String waitTime){
        this.waitTime = waitTime;
    }

        public String getTitle () {
        return title;
    }

        public void setTitle (String title){
        this.title = title;
    }
    }
