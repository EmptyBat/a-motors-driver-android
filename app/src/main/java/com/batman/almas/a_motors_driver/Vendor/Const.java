package com.batman.almas.a_motors_driver.Vendor;

public class Const {
    public static final String DOMAIN_URL = "http://188.166.117.195/";
    public static final String BASE_URL = "http://188.166.117.195/api/v1/";
    public static final String LOGIN_URL = BASE_URL + "driver/login";
    public static final String ORDERS_URL = BASE_URL + "orders_pull?city_id=%s&driver_id=%s";
    public static final String ORDER_URL = BASE_URL + "orders/";
    public static final String DETAIL_URL =  "?with[]=driver&with[]=company&with[]=driver.car&with[]=passanger&with[]=driver.car.brand&with[]=driver.car.color";
    public static final String PUSHER_AUTH_URL = BASE_URL + "pusher-auth";
    public static final String ORDER_COMPLETE_URL =  "/finish";
    public static final String SEND_TOKEN_URL =  BASE_URL + "device_tokens";
    public static final String ORDER_HISTORY_URL =  BASE_URL + "drivers/%s/history?limit=%s";
    public static final String GET_NEW_PASSWORD_URL =  BASE_URL + "driver/password";
    public static final String GET_DRIVER_STATUS =  BASE_URL + "drivers/";
    public static final String FORCE_FINISH =  BASE_URL + "orders/%s/force_finish/";
    public static final String GET_CONFIGS_URL =  BASE_URL + "configs";

}
