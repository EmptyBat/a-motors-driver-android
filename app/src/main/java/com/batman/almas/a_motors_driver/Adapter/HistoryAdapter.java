package com.batman.almas.a_motors_driver.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.batman.almas.a_motors_driver.Model.AboutTaxiModel;
import com.batman.almas.a_motors_driver.Model.DataCasualModel;
import com.batman.almas.a_motors_driver.Model.DetailTripModel;
import com.batman.almas.a_motors_driver.Model.DistrictList;
import com.batman.almas.a_motors_driver.Model.StatsModel;
import com.batman.almas.a_motors_driver.R;
import com.batman.almas.a_motors_driver.TripDetailActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class HistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<DetailTripModel> dataSet;
    private List<DistrictList> districts_list;
    private List<DetailTripModel> orderList;
    private List<Integer> itemController;
    Context mContext;
    int total_types;
    public static class DistrictFromViewHolder extends RecyclerView.ViewHolder {


        TextView district_from_tv;
        TextView started_at_tv;
        TextView finished_at_tv;
        TextView date_tv;
        TextView stats_tv;

        public DistrictFromViewHolder(View itemView) {
            super(itemView);
            this.started_at_tv = (TextView) itemView.findViewById(R.id.start_time_tv);
            this.finished_at_tv = (TextView) itemView.findViewById(R.id.end_time_tv);
            this.date_tv = (TextView) itemView.findViewById(R.id.date_tv);
            this.district_from_tv = (TextView) itemView.findViewById(R.id.district_tv);
            this.stats_tv = (TextView) itemView.findViewById(R.id.stats_tv);
        }

    }

    public static class DistrictToViewHolder extends RecyclerView.ViewHolder {


        TextView district_to_tv;

        public DistrictToViewHolder(View itemView) {
            super(itemView);

            this.district_to_tv = (TextView) itemView.findViewById(R.id.district_tv);

        }

    }

    public static class AdditionalDistrictViewHolder extends RecyclerView.ViewHolder {


        TextView district_tv;
        TextView stats_tv;
        public AdditionalDistrictViewHolder(View itemView) {
            super(itemView);

            this.district_tv = (TextView) itemView.findViewById(R.id.district_tv);
            this.stats_tv = (TextView) itemView.findViewById(R.id.stats_tv);
        }

    }

    public static class history_company_titleViewHolder extends RecyclerView.ViewHolder {


        TextView title_tv;

        public history_company_titleViewHolder(View itemView) {
            super(itemView);

            this.title_tv = (TextView) itemView.findViewById(R.id.title_tv);

        }

    }

    public HistoryAdapter(List<DetailTripModel> orderList, List<DetailTripModel> data, List<DistrictList> districts_list, Context context, List<Integer> itemController) {
        this.dataSet = data;
        this.mContext = context;
        this.districts_list = districts_list;
        this.orderList = orderList;
        total_types = dataSet.size();
        this.itemController = itemController;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case DetailTripModel.COMPANY_NAME_TITLE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.history_company_title_item, parent, false);
                return new HistoryAdapter.history_company_titleViewHolder(view);
            case DetailTripModel.DISTRICT_FROM_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.history_from_item, parent, false);
                return new HistoryAdapter.DistrictFromViewHolder(view);
            case DetailTripModel.ADDITIONA_DISTRICT_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.history_additional_to, parent, false);
                return new HistoryAdapter.AdditionalDistrictViewHolder(view);
            case DetailTripModel.DISTRICT_TO_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.history_to_item, parent, false);
                return new HistoryAdapter.DistrictToViewHolder(view);
        }
        return null;

    }


    @Override
    public int getItemViewType(int position) {

        switch (dataSet.get(position).getDistrict_type()) {
            case 0:
                return DetailTripModel.COMPANY_NAME_TITLE;
            case 1:
                return DetailTripModel.DISTRICT_FROM_TYPE;
            case 2:
                return DetailTripModel.ADDITIONA_DISTRICT_TYPE;
            case 3:
                return DetailTripModel.DISTRICT_TO_TYPE;
            default:
                return 0;
        }


    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int listPosition) {
        DetailTripModel object = dataSet.get(listPosition);
        if (object != null) {
            switch (object.getDistrict_type()) {
                case DetailTripModel.COMPANY_NAME_TITLE:
                    DetailTripModel districtModel = orderList.get(itemController.get(listPosition));
                    ((HistoryAdapter.history_company_titleViewHolder) holder).title_tv.setText(districtModel.getCompany_name());
                    break;
                case DetailTripModel.DISTRICT_FROM_TYPE:
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                    Date currentTime = Calendar.getInstance().getTime();
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(currentTime);
                    int hours = cal.get(Calendar.HOUR_OF_DAY);
                    int minutes = cal.get(Calendar.MINUTE);
                    SimpleDateFormat global_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    SimpleDateFormat format_time = new SimpleDateFormat("HH:mm");
                    try {
                        DetailTripModel districtModels = orderList.get(itemController.get(listPosition-1));
                        Date date = format.parse(districtModels.getStarted_at());
                        Date date_time = global_format.parse(districtModels.getStarted_at());
                        Date date_finishet = global_format.parse(districtModels.getFinished_at());
                        ((HistoryAdapter.DistrictFromViewHolder) holder).date_tv.setText(format.format(date));
                        ((HistoryAdapter.DistrictFromViewHolder) holder).started_at_tv.setText(format_time.format(date_time) + " - ");
                        ((HistoryAdapter.DistrictFromViewHolder) holder).finished_at_tv.setText(format_time.format(date_finishet));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    final DistrictList district_from = districts_list.get(listPosition - itemController.get(listPosition));
                    DistrictList district_from_stats = districts_list.get(listPosition - itemController.get(listPosition)+1);
                    ((HistoryAdapter.DistrictFromViewHolder) holder).district_from_tv.setText(district_from.getName());
                    ((DistrictFromViewHolder) holder).stats_tv.setText(district_from_stats.getTime());
                     String time =  district_from_stats.getTime();
                     String distance =  district_from_stats.getDistance();
                     if (time == null){
                          time = "0";
                     }
                     if (distance == null){
                         distance = "0";
                     }
                    if (Integer.valueOf(time) >60){
                        ((DistrictFromViewHolder) holder).stats_tv.setText((Integer.valueOf(distance)/1000)+ " км " +(time) + " мин" );
                    }
                    else {
                        ((DistrictFromViewHolder) holder).stats_tv.setText((Integer.valueOf(distance)/1000)+ " км " +(time) + " сек" );
                    }
                    ((DistrictFromViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(mContext, TripDetailActivity.class);
                            intent.putExtra("ORDER_ID", district_from.getOrder_id());
                            intent.putExtra("HIDE",true);
                            mContext.startActivity(intent);
                        }
                    });

                    break;
                case DetailTripModel.ADDITIONA_DISTRICT_TYPE:
                    final DistrictList district_additional = districts_list.get(listPosition - itemController.get(listPosition));
                    DistrictList district_additional_stats = districts_list.get(listPosition - itemController.get(listPosition)+1);
                    ((HistoryAdapter.AdditionalDistrictViewHolder) holder).district_tv.setText(district_additional.getName());
                    String time_add =  district_additional_stats.getTime();
                    String distance_add =  district_additional_stats.getDistance();
                    if (time_add == null){
                        time_add = "0";
                    }
                    if (distance_add == null){
                        distance_add = "0";
                    }
                    if (Integer.valueOf(time_add) >60){
                        ((AdditionalDistrictViewHolder) holder).stats_tv.setText((Integer.valueOf(distance_add)/1000)+ " км " +(time_add) + " мин" );
                    }
                    else {
                        ((AdditionalDistrictViewHolder) holder).stats_tv.setText((Integer.valueOf(distance_add)/1000)+ " км " +(time_add) + " сек" );
                    }
                    ((AdditionalDistrictViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(mContext, TripDetailActivity.class);
                            intent.putExtra("ORDER_ID", district_additional.getOrder_id());
                            intent.putExtra("HIDE",true);
                            mContext.startActivity(intent);
                        }
                    });
                    break;
                case DetailTripModel.DISTRICT_TO_TYPE:
                   final DistrictList district_to = districts_list.get(listPosition - itemController.get(listPosition));
                    ((HistoryAdapter.DistrictToViewHolder) holder).district_to_tv.setText(district_to.getName());
                    ((DistrictToViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(mContext, TripDetailActivity.class);
                            intent.putExtra("ORDER_ID", district_to.getOrder_id());
                            intent.putExtra("HIDE",true);
                            mContext.startActivity(intent);
                        }
                    });
                    break;

            }
        }
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }
}