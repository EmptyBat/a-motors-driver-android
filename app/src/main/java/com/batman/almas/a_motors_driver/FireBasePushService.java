package com.batman.almas.a_motors_driver;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

public class FireBasePushService extends FirebaseMessagingService {
    public static final String TAG = "FireBase cloud";
    SharedPreferences prefs;
    public FireBasePushService() {
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // ...
        prefs = getSharedPreferences(Preferences.shPref, 0);
        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        Map<String, String> data = remoteMessage.getData();
        Log.d(TAG, "Message data payload : " + remoteMessage.getData());
        Log.d(TAG, "Message data payload: " + remoteMessage.getNotification().getBody());
        String driver_id = data.get("driver_id");
        String passanger_name = data.get("passanger_name");
        String order_id = data.get("order_id");
        String status_id = data.get("status_id");
        Log.d("afsfsaa","afsasfafs" + driver_id);
        if (prefs.getString(Preferences.user_id, null).equals(driver_id)) {
            if (status_id.equals("4")||status_id.equals("5")) {
                Log.d("asffsafs","2e2faffsa ff" + driver_id);
                Intent intent = new Intent(getBaseContext(), OrdersActivity.class);
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean("driveIsBusy",false);

                SharedPreferences shared = getSharedPreferences(Preferences.shPref, 0);
                SharedPreferences.Editor edit = shared.edit();
                edit.putBoolean(Preferences.arrivedTo,false);
                edit.commit();
                editor.commit();
                startActivity(intent);
            }
            else {
                Intent intent = new Intent(getBaseContext(), MainActivity.class);
                intent.putExtra("ORDER_ID", order_id);
                intent.putExtra("PASSANGER_NAME",passanger_name);
                intent.putExtra("DRIVER_ID",driver_id);
                intent.putExtra("STATUS_ID",status_id);
                startActivity(intent);

            }
        }
        else {
        }
        String click_action = remoteMessage.getNotification().getClickAction();
        Log.d("asffsafs","afsasffasfas" + click_action);
        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getNotification().getBody());
            if (/* Check if data needs to be processed by long running job */ true) {
                // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
          //      scheduleJob();
            } else {
                // Handle message within 10 seconds
          //      handleNow();
            }
        }
        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }
        Intent intent = new Intent(click_action);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);
        notificationBuilder.setSmallIcon(R.drawable.ic_launcher_background)
                .setContentTitle(remoteMessage.getNotification().getTitle())
                .setContentText(remoteMessage.getNotification().getBody())
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);


        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());
        sendNotification(remoteMessage.getNotification().getBody(), remoteMessage.getNotification().getTitle(),driver_id,passanger_name,order_id, click_action);
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("0", "Default channel", NotificationManager.IMPORTANCE_DEFAULT);
            manager.createNotificationChannel(channel);
        }
        manager.notify(0, notificationBuilder.build());
    }
    private void sendNotification(String messageBody, String messageTitle, String driver_id, String passanger_name, String order_id, String click_action) {
        Intent intent = new Intent(click_action);
        if (prefs.getString(Preferences.user_id, null) == driver_id) {
            intent.putExtra("DRIVER_ID", driver_id);
            intent.putExtra("PASSANGER_NAME", passanger_name);
            intent.putExtra("ORDER_ID", order_id);
        }
        else {
            intent.putExtra("PASSANGER_NAME", passanger_name);
            intent.putExtra("ORDER_ID", order_id);
        }
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent,
                PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);
        notificationBuilder.setSmallIcon(R.drawable.ic_launcher_background)
                .setContentTitle(messageTitle)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());
    }

}
