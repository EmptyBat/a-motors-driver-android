package com.batman.almas.a_motors_driver.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.batman.almas.a_motors_driver.Model.AboutTaxiModel;
import com.batman.almas.a_motors_driver.Model.DataCasualModel;
import com.batman.almas.a_motors_driver.Model.DetailTripModel;
import com.batman.almas.a_motors_driver.Model.DistrictList;
import com.batman.almas.a_motors_driver.Model.DistrictModel;
import com.batman.almas.a_motors_driver.Model.StatsModel;
import com.batman.almas.a_motors_driver.R;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DetailTripAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<DetailTripModel> dataSet;
    private List<DistrictList> districts_list;
    private List<DetailTripModel> orderList;
    List<AboutTaxiModel> aboutTaxiModels;
    List<DataCasualModel> data_casual;
    List<StatsModel> total_stats;
    List<StatsModel> stats_add;
    Context mContext;
    int total_types;

    public static class DistrictFromViewHolder extends RecyclerView.ViewHolder {

        TextView district_from_tv;
        TextView started_at_tv;
        TextView finished_at_tv;
        TextView date_tv;
        TextView stats_tv;

        public DistrictFromViewHolder(View itemView) {
            super(itemView);
            this.started_at_tv = (TextView) itemView.findViewById(R.id.start_time_tv);
            this.finished_at_tv = (TextView) itemView.findViewById(R.id.end_time_tv);
            this.date_tv = (TextView) itemView.findViewById(R.id.date_tv);
            this.district_from_tv = (TextView) itemView.findViewById(R.id.district_tv);
            this.stats_tv = (TextView) itemView.findViewById(R.id.stats_tv);
        }

    }

    public static class DistrictToViewHolder extends RecyclerView.ViewHolder {


        TextView district_to_tv;
        TextView total_stats_tv;

        public DistrictToViewHolder (View itemView) {
            super(itemView);

            this.district_to_tv = (TextView) itemView.findViewById(R.id.district_tv);
            this.total_stats_tv = (TextView) itemView.findViewById(R.id.total_stats_tv);

        }

    }

    public static class AdditionalDistrictViewHolder extends RecyclerView.ViewHolder {


        TextView district_tv;

        TextView stats_tv;
        public AdditionalDistrictViewHolder(View itemView) {
            super(itemView);

            this.district_tv = (TextView) itemView.findViewById(R.id.district_tv);
            this.stats_tv = (TextView) itemView.findViewById(R.id.stats_tv);
        }

    }

    public static class history_company_titleViewHolder extends RecyclerView.ViewHolder {


        TextView title_tv;

        public history_company_titleViewHolder(View itemView) {
            super(itemView);

            this.title_tv = (TextView) itemView.findViewById(R.id.title_tv);

        }

    }
    public static class waiting_titleViewHolder extends RecyclerView.ViewHolder {


        TextView title_tv;

        public waiting_titleViewHolder(View itemView) {
            super(itemView);

            this.title_tv = (TextView) itemView.findViewById(R.id.title_tv);

        }

    }
    public static class waiting_itemViewHolder extends RecyclerView.ViewHolder {


         TextView title_tv;
         TextView stats_tv;

        public waiting_itemViewHolder(View itemView) {
            super(itemView);
              this.stats_tv = (TextView) itemView.findViewById(R.id.stats_tv);
              this.title_tv = (TextView) itemView.findViewById(R.id.district_tv);

        }

    }

    public static class AboutTaxiViewHolder extends RecyclerView.ViewHolder {


         TextView driver_tv;
         TextView mark_tv;
         TextView color_tv;
         TextView number_tv;
         Button color_btn;


        public AboutTaxiViewHolder(View itemView) {
            super(itemView);

              this.driver_tv = (TextView) itemView.findViewById(R.id.driver_tv);
              this.mark_tv = (TextView) itemView.findViewById(R.id.mark_tv);
              this.color_tv = (TextView) itemView.findViewById(R.id.color_tv);
              this.number_tv = (TextView) itemView.findViewById(R.id.number_tv);
              this.color_btn = (Button) itemView.findViewById(R.id.color_btn);

        }

    }
    public DetailTripAdapter(List<DetailTripModel> orderList, List<DetailTripModel> data, List<DistrictList> districts_list, List<AboutTaxiModel> aboutTaxiModels, List<DataCasualModel> data_casual,List<StatsModel>total_stats, List<StatsModel> stats_add, Context context) {
        this.dataSet = data;
        this.stats_add = stats_add;
        this.mContext = context;
        this.districts_list = districts_list;
        this.orderList = orderList;
        this.aboutTaxiModels = aboutTaxiModels;
        this.data_casual = data_casual;
        this.total_stats = total_stats;
        total_types = dataSet.size();
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view;
            switch (viewType) {
                case DetailTripModel.COMPANY_NAME_TITLE:
                    view = LayoutInflater.from(parent.getContext()).inflate(R.layout.history_company_title_item, parent, false);
                    return new DetailTripAdapter.history_company_titleViewHolder(view);
                case DetailTripModel.DISTRICT_FROM_TYPE:
                    view = LayoutInflater.from(parent.getContext()).inflate(R.layout.history_from_item, parent, false);
                    return new DetailTripAdapter.DistrictFromViewHolder(view);
                case DetailTripModel.ADDITIONA_DISTRICT_TYPE:
                    view = LayoutInflater.from(parent.getContext()).inflate(R.layout.history_additional_to, parent, false);
                    return new DetailTripAdapter.AdditionalDistrictViewHolder(view);
                case DetailTripModel.DISTRICT_TO_TYPE:
                    view = LayoutInflater.from(parent.getContext()).inflate(R.layout.history_to_item, parent, false);
                    return new DetailTripAdapter.DistrictToViewHolder(view);
                case DetailTripModel.WAITING_TITLE_TYPE:
                    view = LayoutInflater.from(parent.getContext()).inflate(R.layout.history_company_title_item, parent, false);
                    return new DetailTripAdapter.waiting_titleViewHolder(view);
                case DetailTripModel.WAITING_TYPE:
                    view = LayoutInflater.from(parent.getContext()).inflate(R.layout.history_waiting_item, parent, false);
                    return new DetailTripAdapter.waiting_itemViewHolder(view);
                case DetailTripModel.ABOUT_TAXI:
                    view = LayoutInflater.from(parent.getContext()).inflate(R.layout.history_taxi_info, parent, false);
                    return new DetailTripAdapter.AboutTaxiViewHolder(view);
            }
        return null;

    }


    @Override
    public int getItemViewType(int position) {

        switch (dataSet.get(position).getDistrict_type()) {
            case 0:
                return DetailTripModel.COMPANY_NAME_TITLE;
            case 1:
                return DetailTripModel.DISTRICT_FROM_TYPE;
            case 2:
                return DetailTripModel.ADDITIONA_DISTRICT_TYPE;
            case 3:
                return DetailTripModel.DISTRICT_TO_TYPE;
            case 4:
                return DetailTripModel.WAITING_TITLE_TYPE;
            case 5:
                return DetailTripModel.WAITING_TYPE;
            case 6:
                return DetailTripModel.ABOUT_TAXI;
            default:
                return 0;
        }


    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int listPosition) {

        DetailTripModel object = dataSet.get(listPosition);
        DetailTripModel districtModel = orderList.get(0);
            if (object != null) {
                switch (object.getDistrict_type()) {
                    case DetailTripModel.COMPANY_NAME_TITLE:
                        ((history_company_titleViewHolder) holder).title_tv.setText(districtModel.getCompany_name());
                        break;
                    case DetailTripModel.DISTRICT_FROM_TYPE:
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                        Date currentTime = Calendar.getInstance().getTime();
                        Calendar cal = Calendar.getInstance();
                        cal.setTime(currentTime);
                        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                        String currentDateandTime = sdf.format(new Date());
                        SimpleDateFormat global_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        SimpleDateFormat format_time = new SimpleDateFormat("HH:mm");
                        try {
                            Date date = format.parse(districtModel.getStarted_at());
                            Date date_time = global_format.parse(districtModel.getStarted_at());
                            ((DistrictFromViewHolder) holder).date_tv.setText(format.format(date));
                            ((DistrictFromViewHolder) holder).started_at_tv.setText(format_time.format(date_time)+" - ");
                            ((DistrictFromViewHolder) holder).finished_at_tv.setText(currentDateandTime);

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        DistrictList district_from_name = districts_list.get(listPosition -1 );
                         StatsModel district_from = stats_add.get(listPosition -1 );
                        ((DistrictFromViewHolder) holder).district_from_tv.setText(district_from_name.getName());
                        String time =  district_from.getTime_wait();
                        String distance =  district_from.getDistance();
                        if (time == null){
                            time = "0";
                        }
                        if (distance == null){
                            distance = "0";
                        }
                        if ((Integer.valueOf(time)) >60){
                            ((DistrictFromViewHolder) holder).stats_tv.setText((Integer.valueOf(distance)/1000)+ " км " + ((Integer.valueOf(time)) /60) + " мин" );
                        }
                        else {
                            ((DistrictFromViewHolder) holder).stats_tv.setText((Integer.valueOf(distance)/1000)+ " км " +(time) + " сек" );
                        }
                        break;
                    case DetailTripModel.ADDITIONA_DISTRICT_TYPE:
                        DistrictList district_additional_name = districts_list.get(listPosition -1 );
                        StatsModel district_additional = stats_add.get(listPosition -1 );
                        ((DetailTripAdapter.AdditionalDistrictViewHolder) holder).district_tv.setText(district_additional_name.getName());
                        String time_add =  district_additional.getTime_wait();
                        String distance_add =  district_additional.getDistance();
                        if (time_add == null){
                            time_add = "0";
                        }
                        if (distance_add == null){
                            distance_add = "0";
                        }
                        if ((Integer.valueOf(time_add)) >60){
                            ((AdditionalDistrictViewHolder) holder).stats_tv.setText((Integer.valueOf(distance_add)/1000)+ " км " +((Integer.valueOf(time_add)) /60) + " мин" );
                        }
                        else {
                            ((AdditionalDistrictViewHolder) holder).stats_tv.setText((Integer.valueOf(distance_add)/1000)+ " км " +(time_add) + " сек" );
                        }

                        break;
                    case DetailTripModel.DISTRICT_TO_TYPE:
                        StatsModel stats = total_stats.get(0);
                        DistrictList district_to = districts_list.get(listPosition -1 );
                        ((DistrictToViewHolder) holder).district_to_tv.setText(district_to.getName());
                        ((DistrictToViewHolder) holder).total_stats_tv.setText("Время в пути " +((Integer.valueOf(stats.getTime_drive())/60))+ " минут\nвремя ожидания " + (Integer.valueOf(stats.getTime_wait())) + " минут \nрасстояние "+ ((Integer.valueOf(stats.getDistance())/1000))+ " км");
                        break;
                    case DetailTripModel.WAITING_TITLE_TYPE:
                        ((waiting_titleViewHolder) holder).title_tv.setText("Дополнительные точки");
                        break;
                    case DetailTripModel.WAITING_TYPE:
                        DataCasualModel dataCasual = data_casual.get(listPosition - districts_list.size() -2);
                        ((waiting_itemViewHolder) holder).title_tv.setText(dataCasual.getTitle());
                        Integer waitTime = Integer.valueOf(dataCasual.getWaitTime())/60;
                        if (waitTime>=1) {
                            ((waiting_itemViewHolder) holder).stats_tv.setText(waitTime + "  минут");
                        }
                        else {
                            ((waiting_itemViewHolder) holder).stats_tv.setText("1 минута");
                        }

                        break;
                    case DetailTripModel.ABOUT_TAXI:
                        AboutTaxiModel taximodel = aboutTaxiModels.get(0);
                        ((AboutTaxiViewHolder) holder).mark_tv.setText(taximodel.getBrand_name());
                        ((AboutTaxiViewHolder) holder).number_tv.setText(taximodel.getCar_registration_number());
                        ((AboutTaxiViewHolder) holder).driver_tv.setText(taximodel.getDriver_name() +" "+ taximodel.getDriver_surname());
                        ((AboutTaxiViewHolder) holder).color_tv.setText(taximodel.getColor_name());
                        ((AboutTaxiViewHolder) holder).color_btn.setBackgroundColor(Color.parseColor(taximodel.getColor_hexcode()));
                        break;

                }
            }

    }

    @Override
    public int getItemCount() {
            return dataSet.size();
    }

}
