package com.batman.almas.a_motors_driver;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoadingActivity extends AppCompatActivity {
    @BindView(R.id.loading_tv)
    TextView loading_tv;

    @BindView(R.id.loading_back_btn)
    Button loading_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidNetworking.initialize(getApplicationContext());
        setContentView(R.layout.activity_loading);
        ButterKnife.bind(this);
        handler.postDelayed(runnable, 1000);
        loading_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
    final Handler handler = new Handler();
    Runnable runnable = new Runnable() {
        int count = 0;
        @Override
        public void run() {
            count++;

            if (count == 1) {
                loading_tv.setText("Авторизация.");
            } else if (count == 2) {
                loading_tv.setText("Авторизация..");
            } else if (count == 3) {
                loading_tv.setText("Авторизация...");
            }

            if (count == 3)
                count = 0;

            handler.postDelayed(this, 2 * 1000);
        }
    };

}