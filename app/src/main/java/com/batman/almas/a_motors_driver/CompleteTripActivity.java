package com.batman.almas.a_motors_driver;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.batman.almas.a_motors_driver.Vendor.Const;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CompleteTripActivity extends AppCompatActivity {
    public static final String TAG = LoginActivity.class.getSimpleName();
    String order_id;
    @BindView(R.id.code_tv)
    EditText code_tv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidNetworking.initialize(getApplicationContext());
        setContentView(R.layout.activity_complete_trip);
        ButterKnife.bind(this);
        handleData();
        code_tv.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                Log.d("afterTextChanged","afsasf" + (String.valueOf(s.length())));
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                Log.d("beforeTextChanged","afsasf" + (String.valueOf(s.length())));
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                Log.d("onTextChanged","afsasf" + (String.valueOf(s.length())));
                if (code_tv.length() == 4){
                    accept_btnClicked();
                }
            }
        });
    }

    private void handleData() {
        String order_id = getIntent().getStringExtra("ORDER_ID");
        this.order_id = order_id;
    }
    @OnClick(R.id.accept_btn)
    public void accept_btnClicked(){
        SharedPreferences prefs = getSharedPreferences(Preferences.shPref, 0);
        Map<String, String> params = new HashMap<>();
        params.put("confirmation_code", code_tv.getText().toString());
        params.put("driver_id", prefs.getString(Preferences.user_id, null));
        AndroidNetworking.post(Const.ORDER_URL + order_id + Const.ORDER_COMPLETE_URL)
                .addBodyParameter(params)
                .addHeaders("Authorization","Basic ZGV2QGFtb3RvcnMua3o6MTIzMTIz")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getJSONObject("data") == null) {
                                Toast.makeText(CompleteTripActivity.this, getString(R.string.code_wrong), Toast.LENGTH_SHORT).show();

                            } else {
                                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putBoolean("driveIsBusy",false);
                                SharedPreferences shared = getSharedPreferences(Preferences.shPref, 0);
                                SharedPreferences.Editor edit = shared.edit();
                                edit.putBoolean(Preferences.arrivedTo,false);
                                editor.commit();
                                edit.commit();
                                Intent newAct = new Intent(getApplicationContext(), OrdersActivity.class);
                                startActivity(newAct);
                                Toast.makeText(CompleteTripActivity.this, getString(R.string.order_completed), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        Log.i(TAG, anError.getErrorBody().toString());
                        Log.i(TAG, anError.getErrorDetail().toString());
                        Log.i(TAG, String.valueOf(anError.getErrorCode()));
                        Log.i(TAG, anError.getResponse().toString());
                        Toast.makeText(CompleteTripActivity.this, getString(R.string.code_wrong), Toast.LENGTH_SHORT).show();
                        Log.i(TAG, "HERE2");
                    }
                });
    }
    @OnClick(R.id.cancel_btn)
    public void cancel_btnClicked(){
        finish();
    }
}

