package com.batman.almas.a_motors_driver.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.batman.almas.a_motors_driver.Fragment.HistoryFragment;

import java.util.ArrayList;
import java.util.List;

public class SectionPageAdapter  extends FragmentPagerAdapter {

    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();

    public void addFragment(Fragment fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }

    public SectionPageAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }

    @Override
    public Fragment getItem(int position) {
        return HistoryFragment.getInstance(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }
}
