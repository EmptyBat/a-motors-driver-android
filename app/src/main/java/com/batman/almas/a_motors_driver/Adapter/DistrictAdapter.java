package com.batman.almas.a_motors_driver.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.batman.almas.a_motors_driver.Model.DistrictList;
import com.batman.almas.a_motors_driver.Model.DistrictModel;
import com.batman.almas.a_motors_driver.R;
import com.batman.almas.a_motors_driver.TripDetailActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class DistrictAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

private List<DistrictModel> dataSet;
private List<Integer> colorList;
private List<DistrictList> districts_list;
private List<DistrictModel> orderList;
        Context mContext;
        int total_types;
        boolean status;
    DistrictAdapter.DistrictAdapterListener listener;

public static class DistrictToViewHolder extends RecyclerView.ViewHolder {


    TextView district_to_tv;

    public DistrictToViewHolder (View itemView) {
        super(itemView);

        this.district_to_tv = (TextView) itemView.findViewById(R.id.district_tv);

    }

}

public static class PassangerNameViewHolder extends RecyclerView.ViewHolder {


    TextView passanger_name_tv;
    TextView order_time;

    public PassangerNameViewHolder(View itemView) {
        super(itemView);

        this.passanger_name_tv = (TextView) itemView.findViewById(R.id.passanger_name_tv);
        this.order_time = (TextView) itemView.findViewById(R.id.order_time);

    }

}

public static class AdditionalDistrictViewHolder extends RecyclerView.ViewHolder {


    TextView district_tv;
    Button drop_down_btn;

    public AdditionalDistrictViewHolder(View itemView) {
        super(itemView);
        this.drop_down_btn = (Button) itemView.findViewById(R.id.drop_down_btn);
        this.district_tv = (TextView) itemView.findViewById(R.id.district_tv);

    }

}

    public DistrictAdapter(List<DistrictModel> orderList,List<DistrictModel> data,  List<Integer> colorList,List<DistrictList> districts_list, Context context,boolean status,DistrictAdapter.DistrictAdapterListener listener) {
        this.dataSet = data;
        this.mContext = context;
        this.colorList = colorList;
        this.districts_list = districts_list;
        this.orderList = orderList;
        this.status = status;
        this.listener = listener;
        total_types = dataSet.size();

    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(status == true) {
            View view;
            switch (viewType) {
                case DistrictModel.NAME_TYPE:
                    view = LayoutInflater.from(parent.getContext()).inflate(R.layout.passanger_name_item, parent, false);
                    return new PassangerNameViewHolder(view);
                case DistrictModel.ADDITIONA_DISTRICT_TYPE:
                    view = LayoutInflater.from(parent.getContext()).inflate(R.layout.district_additional_item, parent, false);
                    return new AdditionalDistrictViewHolder(view);
                case DistrictModel.DISTRICT_TO_TYPE:
                    view = LayoutInflater.from(parent.getContext()).inflate(R.layout.district_to_item, parent, false);
                    return new DistrictToViewHolder(view);

            }
        }
        else {
            View view;
            switch (viewType) {
                case DistrictModel.NAME_TYPE:
                    view = LayoutInflater.from(parent.getContext()).inflate(R.layout.passanger_name_item, parent, false);
                    return new PassangerNameViewHolder(view);
                default:
                    view = LayoutInflater.from(parent.getContext()).inflate(R.layout.district_to_item, parent, false);
                    return new DistrictToViewHolder(view);

            }

        }
        return null;

    }
    @Override
    public int getItemViewType(int position) {

        switch (dataSet.get(position).getDistrict_type()) {
            case 0:
                return DistrictModel.NAME_TYPE;
            case 1:
                return DistrictModel.ADDITIONA_DISTRICT_TYPE;
            case 2:
                return DistrictModel.DISTRICT_TO_TYPE;
            default:
                return 0;
        }


    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int listPosition) {

        DistrictModel object = dataSet.get(listPosition);
        DistrictModel districtModel = orderList.get(0);
        if(status == true) {
            if (object != null) {
                switch (object.getDistrict_type()) {
                    case DistrictModel.NAME_TYPE:
                        ((PassangerNameViewHolder) holder).passanger_name_tv.setText(districtModel.getPassanger_name() + " (" + districtModel.getCompany_name() + ")");
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                        Date currentTime = Calendar.getInstance().getTime();
                        Calendar cal = Calendar.getInstance();
                        cal.setTime(currentTime);
                        try {
                            Date date = format.parse(districtModel.getOrder_at());

                            ((PassangerNameViewHolder) holder).order_time.setText(format.format(date));;
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        break;
                    case DistrictModel.ADDITIONA_DISTRICT_TYPE:
                        if (listPosition == 1&&districts_list.size()>3){
                            ((AdditionalDistrictViewHolder) holder).drop_down_btn.setVisibility(View.VISIBLE);
                        }
                        ((AdditionalDistrictViewHolder) holder).drop_down_btn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                listener.onModelDataselected(listPosition);
                            }
                        });
                        DistrictList district_additional = districts_list.get(listPosition - 1);
                        Integer current_color = colorList.get(listPosition - 1);
                        ((AdditionalDistrictViewHolder) holder).district_tv.setText(district_additional.getName());
                        ((AdditionalDistrictViewHolder) holder).district_tv.setTextColor(ContextCompat.getColor(mContext, current_color));
                        break;
                    case DistrictModel.DISTRICT_TO_TYPE:
                        Integer current_color_to = colorList.get(listPosition - 1);
                        DistrictList district_to = districts_list.get(listPosition - 1);
                        ((DistrictToViewHolder) holder).district_to_tv.setText(district_to.getName());
                        ((DistrictToViewHolder) holder).district_to_tv.setTextColor(ContextCompat.getColor(mContext, current_color_to));
                        break;
                }
            }
        }
        else {
                switch (object.getDistrict_type()) {
                    case DistrictModel.NAME_TYPE:
                        ((PassangerNameViewHolder) holder).passanger_name_tv.setText(districtModel.getPassanger_name() + " (" + districtModel.getCompany_name() + ")");
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                        Date currentTime = Calendar.getInstance().getTime();
                        Calendar cal = Calendar.getInstance();
                        cal.setTime(currentTime);
                        try {
                            Date date = format.parse(districtModel.getOrder_at());
                            ((PassangerNameViewHolder) holder).order_time.setText(format.format(date));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        break;
                    case DistrictModel.ADDITIONA_DISTRICT_TYPE:
                        Integer current_color_to = colorList.get(listPosition - 1);
                        DistrictList district_to = districts_list.get(listPosition - 1);
                        ((DistrictToViewHolder) holder).district_to_tv.setText(district_to.getName());
                        ((DistrictToViewHolder) holder).district_to_tv.setTextColor(ContextCompat.getColor(mContext, current_color_to));
                        break;
                }

        }

    }

    @Override
    public int getItemCount() {
       if(status == true) {
            return dataSet.size();
        }
        else {
            return 2;
        }
    }
    public interface DistrictAdapterListener {
        void onModelDataselected(int index);
    }


}