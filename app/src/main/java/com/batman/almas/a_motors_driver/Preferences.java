package com.batman.almas.a_motors_driver;

public class Preferences {

    public static final String shPref = "shPref";
    public static final String logged = "logged";
    public static final String checkAccess = "checkAccess";
    public static final String userPhone = "userPhone";
    public static final String userName = "userName";
    public static final String user_id = "user_id";
    public static final String car_id = "car_id";
    public static final String image_url = "image_url";
    public static final String device_token = "device_token";
    public static final String arrivedTo = "arrivedTo";
    public static final String driverStatus = "driverStatus";
    public static final String driverNumber = "driverNumber";
    public static final String frequency_in_order = "frequency_in_order";
    public static final String frequency_without_order = "frequency_without_order";
    public static final String frequency_by_distance = "frequency_by_distance";

}
