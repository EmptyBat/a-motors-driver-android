package com.batman.almas.a_motors_driver;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.batman.almas.a_motors_driver.Vendor.Const;
import com.google.firebase.iid.FirebaseInstanceId;
import com.redmadrobot.inputmask.MaskedTextChangedListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {
    public static final String TAG = LoginActivity.class.getSimpleName();

    @BindView(R.id.phone_tv)
    EditText mPhoneView;
    @BindView(R.id.password_tv)
    EditText mPasswordView;
    @BindView(R.id.sign_btn)
    Button signBtn;
    private boolean code_sended = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidNetworking.initialize(getApplicationContext());
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        checkLogin();
        prepareUI();
        getConfig();
        final MaskedTextChangedListener listener = new MaskedTextChangedListener(
                "+7 ([000]) [000] [00] [00]",
                true,
                mPhoneView,
                null,
                new MaskedTextChangedListener.ValueListener() {
                    @Override
                    public void onTextChanged(boolean maskFilled, @NonNull final String extractedValue) {
                        if(String.valueOf(maskFilled).equals("true")){
                      //      currentUserPhone = "+7" + extractedValue;
                        }
                    }
                }
        );
        mPhoneView.addTextChangedListener(listener);
        mPhoneView.setOnFocusChangeListener(listener);
        mPhoneView.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

                if (mPhoneView.length() == 18){
                    sendNewPassword();
                }

            }
        });
    }
    private void prepareUI() {
        if (ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 200);
                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }
    private void checkLogin(){
        SharedPreferences prefs = getSharedPreferences(Preferences.shPref, 0);
        if (prefs.getString(Preferences.user_id, null) != null) {
            SharedPreferences pref_default = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            boolean driveIsBusy = pref_default.getBoolean("driveIsBusy", false);
            if (driveIsBusy == true) {
                Intent newAct = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(newAct);
                finish();
                }
                else {
                Intent newAct = new Intent(getApplicationContext(), OrdersActivity.class);
                startActivity(newAct);
                finish();
                }
        }
    }
    private void getConfig(){
        AndroidNetworking.get(Const.GET_CONFIGS_URL)
                .addHeaders("Authorization","Basic ZGV2QGFtb3RvcnMua3o6MTIzMTIz")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            SharedPreferences sharedPreferences = getSharedPreferences(Preferences.shPref, 0);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString(Preferences.frequency_by_distance, response.getString("frequency_by_distance"));
                            editor.putString(Preferences.frequency_without_order, response.getString("frequency_without_order"));
                            editor.putString(Preferences.frequency_in_order, response.getString("frequency_in_order"));
                            editor.commit();
                            editor.apply();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.i(TAG, anError.getErrorBody().toString());
                        Log.i(TAG, anError.getErrorDetail().toString());
                        Log.i(TAG, String.valueOf(anError.getErrorCode()));
                        Log.i(TAG, anError.getResponse().toString());
                        Toast.makeText(LoginActivity.this, getString(R.string.app_name), Toast.LENGTH_SHORT).show();
                        Log.i(TAG, "HERE2");
                    }
                });
    }
    @OnClick(R.id.sign_btn)
    public void btnSendCode() {
        Map<String, String> params = new HashMap<>();
        params.put("phone", mPhoneView.getText().toString().replace(" ", ""));
        params.put("password", mPasswordView.getText().toString());
        AndroidNetworking.post(Const.LOGIN_URL)
                .addBodyParameter(params)
                .addHeaders("Authorization","Basic ZGV2QGFtb3RvcnMua3o6MTIzMTIz")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getJSONObject("data") != null) {
                                SharedPreferences sharedPreferences = getSharedPreferences(Preferences.shPref, 0);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString(Preferences.user_id, response.getJSONObject("data").getString("id"));
                                editor.putString(Preferences.userPhone, response.getJSONObject("data").getString("phone"));
                                editor.putString(Preferences.image_url, response.getJSONObject("data").getString("photo"));
                                editor.putString(Preferences.car_id, response.getJSONObject("data").getJSONObject("car").getString("registration_number"));
                                editor.putString(Preferences.userName,response.getJSONObject("data").getString("name") + " " + response.getJSONObject("data").getString("surname"));
                                editor.commit();
                                sendRegistrationToServer();
                                Toast.makeText(LoginActivity.this, getString(R.string.app_name), Toast.LENGTH_SHORT).show();
                                Intent newAct = new Intent(getApplicationContext(), OrdersActivity.class);
                                startActivity(newAct);
                                finish();

                            } else {
                                Toast.makeText(LoginActivity.this, getString(R.string.app_name), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.i(TAG, anError.getErrorBody().toString());
                        Log.i(TAG, anError.getErrorDetail().toString());
                        Log.i(TAG, String.valueOf(anError.getErrorCode()));
                        Log.i(TAG, anError.getResponse().toString());
                        Toast.makeText(LoginActivity.this, getString(R.string.app_name), Toast.LENGTH_SHORT).show();
                        Log.i(TAG, "HERE2");
                    }
                });
    }
    private void sendRegistrationToServer(){
        SharedPreferences prefs = getSharedPreferences(Preferences.shPref, 0);
        Map<String, String> params = new HashMap<>();
        params.put("token",prefs.getString(Preferences.device_token, null));
        params.put("type_id",  "2");
        params.put("entity_id",  prefs.getString(Preferences.user_id, null));
        params.put("entity_type",  "App\\Models\\Driver");
        AndroidNetworking.post(Const.SEND_TOKEN_URL)
                .addBodyParameter(params)
                .addHeaders("Authorization","Basic ZGV2QGFtb3RvcnMua3o6MTIzMTIz")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                    }
                    @Override
                    public void onError(ANError anError) {
                        Log.i(TAG, anError.getErrorBody().toString());
                        Log.i(TAG, anError.getErrorDetail().toString());
                        Log.i(TAG, String.valueOf(anError.getErrorCode()));
                        Log.i(TAG, anError.getResponse().toString());
                    }
                });
    }

    private void sendNewPassword() {
        if (code_sended == false) {
            code_sended = true;
            SharedPreferences prefs = getSharedPreferences(Preferences.shPref, 0);
            Map<String, String> params = new HashMap<>();
            params.put("phone", mPhoneView.getText().toString().replace(" ", ""));
            AndroidNetworking.post(Const.GET_NEW_PASSWORD_URL)
                    .addBodyParameter(params)
                    .addHeaders("Authorization", "Basic ZGV2QGFtb3RvcnMua3o6MTIzMTIz")
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("responce","responce" +response);
                        }
                        @Override
                        public void onError(ANError anError) {
                            Log.i(TAG, anError.getErrorBody().toString());
                            Log.i(TAG, anError.getErrorDetail().toString());
                            Log.i(TAG, String.valueOf(anError.getErrorCode()));
                            Log.i(TAG, anError.getResponse().toString());
                        }
                    });
        }
    }
}