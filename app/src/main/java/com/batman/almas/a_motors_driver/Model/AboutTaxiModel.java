package com.batman.almas.a_motors_driver.Model;

import java.io.Serializable;

public class AboutTaxiModel implements Serializable {
    private String driver_name;
    private String driver_surname;
    private String patronymic;
    private String driver_phone;
    private String car_registration_number;
    private String brand_name;
    private String color_name;
    private String color_hexcode;
    public AboutTaxiModel(){}

    public String getDriver_name() {
        return driver_name;
    }

    public void setDriver_name(String driver_name) {
        this.driver_name = driver_name;
    }

    public String getDriver_surname() {
        return driver_surname;
    }

    public void setDriver_surname(String driver_surname) {
        this.driver_surname = driver_surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getDriver_phone() {
        return driver_phone;
    }

    public void setDriver_phone(String driver_phone) {
        this.driver_phone = driver_phone;
    }

    public String getCar_registration_number() {
        return car_registration_number;
    }

    public void setCar_registration_number(String car_registration_number) {
        this.car_registration_number = car_registration_number;
    }

    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }

    public String getColor_name() {
        return color_name;
    }

    public void setColor_name(String color_name) {
        this.color_name = color_name;
    }

    public String getColor_hexcode() {
        return color_hexcode;
    }

    public void setColor_hexcode(String color_hexcode) {
        this.color_hexcode = color_hexcode;
    }


}
