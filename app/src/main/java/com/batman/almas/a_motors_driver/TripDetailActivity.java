package com.batman.almas.a_motors_driver;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.batman.almas.a_motors_driver.Adapter.DetailTripAdapter;
import com.batman.almas.a_motors_driver.Model.AboutTaxiModel;
import com.batman.almas.a_motors_driver.Model.DataCasualModel;
import com.batman.almas.a_motors_driver.Model.DetailTripModel;
import com.batman.almas.a_motors_driver.Model.DistrictList;
import com.batman.almas.a_motors_driver.Model.DistrictModel;
import com.batman.almas.a_motors_driver.Model.StatsModel;
import com.batman.almas.a_motors_driver.Vendor.Const;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TripDetailActivity extends AppCompatActivity{
    public static final String TAG = LoginActivity.class.getSimpleName();
    @BindView(R.id.detail_rv)
    RecyclerView recyclerView;
    @BindView(R.id.accept_btn)
    Button accept_btn;
    private Fragment fragment;
    private FragmentManager fragmentManager;
    String order_id;
    DetailTripAdapter adapter;
    List<DetailTripModel> itemType;
    List<DistrictList> districtsList;
    List<DetailTripModel> itemList;
    List<AboutTaxiModel> aboutTaxiModels;
    List<DataCasualModel> data_casual;
    List<StatsModel> total_stats;
    List<StatsModel> additional_stats;
    boolean hide = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidNetworking.initialize(getApplicationContext());
        setContentView(R.layout.activity_trip_detail);
        ButterKnife.bind(this);
        handleData();
        prepareUI();
    }
    private void handleData(){
        String order_id= getIntent().getStringExtra("ORDER_ID");
        hide = getIntent().getBooleanExtra("HIDE",false);
        if (hide == true){
            accept_btn.setVisibility(View.INVISIBLE);
            ConstraintLayout.LayoutParams lp = (ConstraintLayout.LayoutParams) accept_btn.getLayoutParams();
            lp.height = 0;
            accept_btn.setLayoutParams(lp);
        }
        this.order_id = order_id;
        getTripinfo();
    }
    private void prepareUI(){
    }
    private void getTripinfo() {
        String url = Const.ORDER_URL + order_id + Const.DETAIL_URL;
        AndroidNetworking.get(url)
                .addHeaders("Authorization","Basic ZGV2QGFtb3RvcnMua3o6MTIzMTIz")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject data = response.getJSONObject("data");
                            DetailTripModel DistrictItemModel = new DetailTripModel();
                            DistrictList districtList = new DistrictList();
                            AboutTaxiModel taximodel = new AboutTaxiModel();
                            StatsModel  addmodel = new StatsModel();
                            DataCasualModel data_casual_object = new DataCasualModel();
                            itemType = new ArrayList<>();
                            itemList = new ArrayList<>();
                            districtsList = new ArrayList<>();
                            aboutTaxiModels = new ArrayList<>();
                            data_casual = new ArrayList<>();
                            total_stats = new ArrayList<>();
                            additional_stats = new ArrayList<>();
                            Date currentTime = Calendar.getInstance().getTime();
                            DistrictItemModel.setCompany_name(data.getJSONObject("company").getString("title"));
                            DistrictItemModel.setPassanger_name(data.getString("passanger_name"));
                            if (data.get("information") instanceof JSONObject) {
                                JSONObject information = data.getJSONObject("information").getJSONObject("exists");
                                if (information.has("data_to")) {
                                    JSONObject stats_value = information.getJSONObject("data_to");
                                    if (stats_value.has("time")) {
                                        JSONObject stats = stats_value.getJSONObject("time");
                                        DistrictItemModel.setStarted_at(stats.getString("started"));
                                        DistrictItemModel.setFinished_at(stats.getString("finished"));
                                    } else {
                                        DistrictItemModel.setStarted_at(data.getString("started_at"));
                                        DistrictItemModel.setFinished_at(currentTime.toString());
                                    }
                                }
                            }
                            itemList.add(DistrictItemModel);
                            itemType.add(new DetailTripModel(DetailTripModel.COMPANY_NAME_TITLE));
                            itemType.add(new DetailTripModel(DetailTripModel.DISTRICT_FROM_TYPE));
                            districtList.setLatitude(data.getJSONObject("data_from").getString("latitude"));
                            districtList.setLongitude(data.getJSONObject("data_from").getString("longitude"));
                            districtList.setName(data.getJSONObject("data_from").getString("name"));
                            JSONObject information1 = data.getJSONObject("information").getJSONObject("exists");
                                JSONObject stats_value1 = information1.getJSONObject("data_to");
                            if (stats_value1.has("stats")) {
                                JSONObject stats = stats_value1.getJSONObject("stats");
                                districtList.setDistance(stats.getString("distance"));
                                districtList.setTime(stats.getString("time"));
                            }
                            else {
                                districtList.setDistance("0");
                                districtList.setTime("0");
                            }
                            districtsList.add(districtList);
                            //About taxi
                            JSONObject driver_object = data.getJSONObject("driver");
                            JSONObject car_object = driver_object.getJSONObject("car");
                            taximodel.setDriver_name(driver_object.getString("name"));
                            taximodel.setDriver_surname(driver_object.getString("surname"));
                            taximodel.setPatronymic(driver_object.getString("patronymic"));
                            taximodel.setBrand_name(car_object.getJSONObject("brand").getString("name"));
                            taximodel.setColor_name(car_object.getJSONObject("color").getString("name"));
                            taximodel.setColor_hexcode(car_object.getJSONObject("color").getString("hex_code"));
                            taximodel.setCar_registration_number(car_object.getString("registration_number"));
                            aboutTaxiModels.add(taximodel);
                            if (data.get("data_additional") instanceof JSONArray)
                            {
                                JSONArray data_additional = data.optJSONArray("data_additional");
                                for (int j = 0; j < data_additional.length(); j++) {
                                    districtList = new DistrictList();
                                    addmodel = new StatsModel();
                                    JSONObject jsonObject = data_additional.getJSONObject(j);
                                    districtList.setName(jsonObject.getString("name"));
                                    districtList.setLongitude(jsonObject.getString("longitude"));
                                    districtList.setLatitude(jsonObject.getString("latitude"));
                                    itemType.add(new DetailTripModel(DetailTripModel.ADDITIONA_DISTRICT_TYPE));
                                    if (data.get("information") instanceof JSONObject) {
                                        JSONObject information = data.getJSONObject("information").getJSONObject("exists");
                                        if (information.has("data_additional_"+(j+1))) {
                                            JSONObject stats_value = information.getJSONObject("data_additional_"+(j+1));
                                            if (stats_value.has("stats")) {
                                                JSONObject stats = stats_value.getJSONObject("stats");
                                                districtList.setDistance(stats.getString("distance"));
                                                districtList.setTime(stats.getString("time"));
                                            }
                                            else {
                                                districtList.setDistance("0");
                                                districtList.setTime("0");
                                            }
                                            if (stats_value.has("stats")) {
                                                JSONObject stats = stats_value.getJSONObject("stats");
                                                addmodel.setDistance(stats.getString("distance"));
                                                addmodel.setTime_wait(stats.getString("time"));
                                            }
                                            else {
                                                addmodel.setDistance("0");
                                                addmodel.setTime_wait("0");
                                            }

                                        }
                                    }
                                    additional_stats.add(addmodel);
                                    districtsList.add(districtList);
                                }
                            }
                            districtList = new DistrictList();
                            addmodel = new StatsModel();
                            districtList.setLatitude(data.getJSONObject("data_to").getString("latitude"));
                            districtList.setLongitude(data.getJSONObject("data_to").getString("longitude"));
                            districtList.setName(data.getJSONObject("data_to").getString("name"));
                            if (data.get("information") instanceof JSONObject) {
                                JSONObject information = data.getJSONObject("information").getJSONObject("exists");
                                if (information.has("data_to")) {
                                    JSONObject stats_value = information.getJSONObject("data_to");
                                    if (stats_value.has("stats")) {
                                        JSONObject stats = stats_value.getJSONObject("stats");
                                        districtList.setDistance(stats.getString("distance"));
                                        districtList.setTime(stats.getString("time"));
                                    }
                                    else {
                                        districtList.setDistance("0");
                                        districtList.setTime("0");
                                    }
                                    if (stats_value.has("stats")) {
                                        JSONObject stats = stats_value.getJSONObject("stats");
                                        addmodel.setDistance(stats.getString("distance"));
                                        addmodel.setTime_wait(stats.getString("time"));
                                    }
                                    else {
                                        addmodel.setDistance("0");
                                        addmodel.setTime_wait("0");
                                    }

                                }
                            }
                            additional_stats.add(addmodel);
                            districtsList.add(districtList);
                            itemType.add(new DetailTripModel(DetailTripModel.DISTRICT_TO_TYPE));
                            if (data.getJSONObject("information").get("casual") instanceof JSONObject)
                            {
                                itemType.add(new DetailTripModel(DetailTripModel.WAITING_TITLE_TYPE));
                                JSONObject data_casualObject= data.getJSONObject("information").getJSONObject("casual");
                                       Iterator<String> keys = data_casualObject.keys();
                                       while (keys.hasNext()) {
                                           String key = keys.next();
                                           if(data_casualObject.get(key) instanceof JSONObject) {
                                               if (data_casualObject.getJSONObject(key).has("stats")) {
                                                   JSONObject stats = data_casualObject.getJSONObject(key).getJSONObject("stats");
                                                   JSONObject name = data_casualObject.getJSONObject(key).getJSONObject("data");
                                                   data_casual_object = new DataCasualModel();
                                                   data_casual_object.setTitle(name.getString("name"));
                                                   data_casual_object.setWaitTime(stats.getString("wait"));
                                                   itemType.add(new DetailTripModel(DetailTripModel.WAITING_TYPE));
                                                   data_casual.add(data_casual_object);
                                               }
                                   }
                                }
                            }
                            if (data.getJSONObject("information").get("stats") instanceof JSONObject) {
                                JSONObject stats =  data.getJSONObject("information").getJSONObject("stats");
                                StatsModel model = new StatsModel();
                                model.setDistance(stats.getString("distance"));
                                model.setTime_drive(stats.getString("time_drive"));
                                model.setTime_wait(stats.getString("time_wait"));
                                total_stats.add(model);
                            }
                            itemType.add(new DetailTripModel(DetailTripModel.ABOUT_TAXI));
                            adapter = new DetailTripAdapter(itemList,itemType,districtsList,aboutTaxiModels,data_casual,total_stats,additional_stats,TripDetailActivity.this);
                            LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
                            llm.setOrientation(LinearLayoutManager.VERTICAL);
                            recyclerView.setLayoutManager(llm);
                            recyclerView.setAdapter(adapter);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(TripDetailActivity.this, "Что-то пошло не так, обратитись к диспетчеру", Toast.LENGTH_SHORT).show();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        Log.i(TAG, anError.getErrorBody().toString());
                        Log.i(TAG, anError.getErrorDetail().toString());
                        Log.i(TAG, String.valueOf(anError.getErrorCode()));
                        Log.i(TAG, anError.getResponse().toString());
                        Toast.makeText(TripDetailActivity.this, "Что-то пошло не так, обратитись к диспетчеру", Toast.LENGTH_SHORT).show();
                    }
                });
    }
    @OnClick(R.id.accept_btn)
    public void accept_btnClicked() {
        Intent newAct = new Intent(getApplicationContext(), CompleteTripActivity.class);
        newAct.putExtra("ORDER_ID", order_id);
        startActivity(newAct);
    }
    @OnClick(R.id.cancel_btn)
    public void cancel_btnClicked(){
        finish();
    }
}
